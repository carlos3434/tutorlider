/**
 * cbpAnimatedHeader.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var cbpAnimatedHeader = (function() {

	var docElem = document.documentElement,
		header = document.querySelector( '.navbar-inverse' ),
		header1 = document.querySelector( '.navbar-brand' ),
		/*header2 = document.querySelector( '.active2' ),		*/
		didScroll = false,
		changeHeaderOn = 50;

	function init() {
		window.addEventListener( 'scroll', function( event ) {
			if( !didScroll ) {
				didScroll = true;
				setTimeout( scrollPage, 250 );
			}
		}, false );
	}

	function scrollPage() {
		var sy = scrollY();
		if ( sy >= changeHeaderOn ) {
			classie.add( header, 'navbar-default' );
			classie.add( header1, 'navbar-brand2' );
			/*classie.add( header2, 'active1' );
			classie.remove( header2, 'active2' );			*/
		}
		else {
			classie.remove( header, 'navbar-default' );
			classie.remove( header1, 'navbar-brand2' );
			/*classie.add( header2, 'active2' );*/
		}
		didScroll = false;
	}

	function scrollY() {
		return window.pageYOffset || docElem.scrollTop;
	}

	init();

})();