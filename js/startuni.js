// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
  $('a.page-scroll').bind('click',function(event){
    var anchor = $(this).attr('href');

    if (anchor!=='' && anchor !='#' && anchor.substring(0, 1)=='#' ) {
      $('html, body').stop().animate({
        scrollTop: $(anchor).offset().top
        //scrollTop: this.offsetTop
      }, 1500);
      event.preventDefault();
    }
  });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy(
    {
      target: '.navbar-fixed-top'
    }
);

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(
    function() {
      $('.navbar-toggle:visible').click();
    }
);

function contratar(precio) {
  $('#precioregistro').val(precio);
  $('#precioregistro2').val(precio);
}

function login() {
  $('#myModal').modal('hide');
  $('#myModal2').modal('show');
}
function registro() {
  $('#myModal2').modal('hide');
  $('#myModal').modal('show');
}
function filtros_Login() {
  var correo = $('#email_registro').val();
  // $('#response_correo').css( "display", "inherit");
  $('#respuesta_correo').load(
      'verificar.php',{correo_registro:correo},function() {
      verficar_correo();
      }
  );
}
function verficar_correo() {
  var a = $('#respuesta_correo').html();
  if (a == "Este correo ya esta siendo usado") {
    $('#response_correo').css("display", "inherit");
    $('#btn_registrar').attr("disabled","disabled");
  }
  else {
    $('#response_correo').css("display", "none");
    $('#btn_registrar').removeAttr("disabled");
  }
}
function desabilitar() {
  $('#btn_registrar').attr('disabled','disabled');
}

/* login facebook */


// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
  FB.login(
      function(response) {
        statusChangeCallback();
      },{scope:'public_profile,email'}
  );
}
function statusChangeCallback() {
  FB.getLoginStatus(
      function(response) {
        var accessToken = response.authResponse.accessToken;
        var precio_contrato = $('#precioregistro').val();
        if(response.status == 'connected') {
          FB.api(
              '/me', function(response) {
                if (precio_contrato == "registro") {
                  location.href = "fbconfig.php?email="+response.email+"&name="+response.first_name+"&apellido="+response.last_name+"&token="+accessToken;
                } else {
                  location.href = "fbconfig.php?email="+response.email+"&name="+response.first_name+"&apellido="+response.last_name+"&token="+accessToken+"&precio="+precio_contrato;
                }
              }
          );
        } else if(response.status == 'not_authorized') {
          alert('Debes autorizar la app!');
        } else {
          alert('Debes ingresar a tu cuenta de Facebook!');
        }
      }
  );
}

window.fbAsyncInit = function() {
  FB.init(
      {
      appId      : '374635696068831',
      cookie     : true,  // enable cookies to allow the server to access
                      // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.3' // use version 2.2
      }
  );

  // Now that we've initialized the JavaScript SDK, we call
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

};

// Load the SDK asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI(accessToken) {
  var precio_contrato = $('#precioregistro').val();
  console.log('Welcome!  Fetching your information.... ');
  FB.api(
      '/me', function(response) {
      console.log('Successful login for: ' + response.name);

      }
  );
}