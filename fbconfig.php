<?php
session_start();
// added in v4.0.0
if (isset($_GET['email']) and isset($_GET['name']) and isset($_GET['token'])) {
  $femail = $_GET['email'];
  $fbfirt_name = (isset($_GET['name'])) ? $_GET['name'] : '' ;
  $fblast_name = (isset($_GET['apellido'])) ? $_GET['apellido'] : '' ;
  $fbtoken = $_GET['token'];
  // graph api request for user data

  include('intranet/conexion.php');
  $consulta = "SELECT * FROM usuarios where correo = '$femail'";
  $resultado = $mysqli->query($consulta);
  $num_row = $resultado->num_rows;
  if ($num_row == 1) {
    while ($fila = $resultado->fetch_row()) :
      $_SESSION['login'] = 'si';
      if (isset($_GET['precio'])) {
        $_SESSION['contrato'] = $_GET['precio'];
      }
      $_SESSION['id'] = $fila[0];
      $_SESSION['nombre'] = $fila[1];
      $_SESSION['apellido'] = $fila[2];
      $_SESSION['correo'] = $fila[3];
      $_SESSION['email_confirmado'] = $fila[6];
      $_SESSION['estado'] = $fila[7];
      $_SESSION['rol_id'] = $fila[8];
      $opciones = "SELECT o.*
                    FROM usuario_opcion up JOIN opciones o on up.opcion_id=o.id
                    where usuario_id = $fila[0] AND up.estado =1 AND o.estado=1";
      $accesos = array();
      if ($resultado2 = $mysqli->query($opciones)) :
        while ($filas = $resultado2->fetch_row()) {
            $accesos[] = $filas;
        }
      endif;
      $_SESSION['accesos'] = $accesos;

    endwhile;
    header("Location: intranet/index.php");
  } elseif ($num_row == 0) { 
    include('intranet/llave.php');
    // generador de token
    function genera_random($longitud)
    {
        $exp_reg="[^A-Z0-9]";
        return substr(
            preg_replace($exp_reg, "", md5(rand())) .
            preg_replace($exp_reg, "", md5(rand())) .
            preg_replace($exp_reg, "", md5(rand())),
            0, $longitud
        );
    }
    $gen_token = genera_random(40);
    $enviar = "INSERT INTO usuarios (nombres, apellido, correo, contrasena, token, email_confirmado, estado, rol_id)
              VALUES ('$fbfirt_name', '$fblast_name', '$femail', AES_ENCRYPT('$fbtoken','$llave'),'$gen_token', 0, 0 ,1)";

    $resultado = $mysqli->query($enviar);  
    $id = $_SESSION['id'] = $mysqli->insert_id;
    $_SESSION['login'] = 'si';
    $_SESSION['nombre'] = $fbfirt_name;
    if ($resultado) {
      $opciones="INSERT INTO usuario_opcion (usuario_id,opcion_id,estado) 
            VALUES ($id,1,1),($id,2,1),($id,3,1),($id,4,1),($id,6,1)";
      $resultado2 = $mysqli->query($opciones);
    }
    $opciones = "SELECT o.*
                FROM usuario_opcion up JOIN opciones o on up.opcion_id=o.id
                where usuario_id = '$id' AND up.estado =1 AND o.estado=1";
    if ($resultado2 = $mysqli->query($opciones)) :
      while ($filas = $resultado2->fetch_row()) {
          $accesos[] = $filas;
      }
    endif;
    $_SESSION['accesos'] = $accesos;

    header("Location: intranet/index.php");
  } else {
    $_SESSION['login'] = "error";
  }
    /* ---- header location after session ----*/
  header("Location: intranet/index.php");
} else {
  header("Location: index.html");
}
