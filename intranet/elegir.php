<?php
$id_usuario = $_SESSION['id'];
?>
<div class="blockcontenido">
  <section class="contenido">
    <span class="encabezado"><a href="index.php">Home /</a> Elegir tutor</span>
  </section>
  <section class="contenido">
    <div class="row">
      <div class="col-xs-12">
          <!-- Inicia contenido -->
          <div class="box">
              <div class="box-body table-responsive">
                  <div class="panel-group" id="t_clases" role="tablist" aria-multiselectable="true">
              </div><!-- /.box-body -->
          </div><!-- /.box -->
          <!-- Finaliza contenido -->
      </div>
    </div>
  </section><!-- /.content -->
  <?php include('form/elegir_tutor_modal.php'); ?>
  <div class="row">
    <div class="col-lg-12">
      <section class="contenido">
        <p>Copyright 2015 © <strong>Tutor Líder</strong></p>
      </section>
    </div>
  </div>
</div>

<script type="text/javascript">
  var id_alumno = "<?php echo $id_usuario; ?>";
  //alert(id_alumno);
</script>
<script type="text/javascript" src="js/elegir_tutor_ajax.js"></script>
<script type="text/javascript" src="js/elegir_tutor.js"></script>