<header class="header">
  <div class="header-logo">
    <img src="imagen/logo.png" alt="Tutor Líder"></div>
  </div>
  <nav class="menu-alertas">
    <a href="#" id="toggle"><i class="icon ion-navicon-round"></i></a>
  </nav>
</header>
<aside id="menu-principal"class="menu-principal">
  <div class="usuario">
    <img src="imagen/avatar.png" alt="avatar" class="img-circle">
    <span><?php  echo $_SESSION['nombre']?></span>
  </div>
  <ul class="menu-navegacion">
    <li class="separador">MENÚ DE NAVEGACIÓN</li>
    <li class="<?php  echo $panel ?>">
      <a href="index.php"><i class="icon ion-home"></i> Panel de control</a>
    </li>
    <?php 
    if (isset($_SESSION['accesos'])) :
      $accesos =$_SESSION['accesos'];
      # code...
      foreach ($accesos as $key => $value) {
        /*
        $value[0]//id
        $value[1]//nombre
        $value[2]//ruta
        $value[3]//titulo
        $value[4]//estado
        $value[5]//icon
        */
        //condicion para saber que menu esta activo
        //se recibira variable de index.php
        //se recibira el nombre de la ruta que esta activo
        if ($titulo==$value[2]) {
          $estadoMenu="activo";
        } else {
          $estadoMenu="";

        }
        ?>
      <li class="<?php  echo $estadoMenu ?>">
        <a href="index.php?title=<?php echo $value[2] ?>"<?php echo $value[2] ?> ><i class="<?php echo $value[5] ?>"></i> <?php echo $value[1] ?></a>
      </li>
      <?php } ?>
    <?php endif ?>
    <li>
      <a href="index.php?sesion=no"><i class="icon ion-power"></i> Salir</a>
    </li>
  </ul>
</aside>
