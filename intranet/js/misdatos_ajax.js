var id_clase, ClasesObj;
var Misdatos={
    Cargar:function(formData,id_usuario){
        $.ajax({
            url         : "php_ajax/admin.php",
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                HTMLCargarMisDatos(obj[0]);
                ClasesObj=obj;
                $('#tutoresModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    },
    Editar:function(formData,id_usuario ){
        $.ajax({
            url         : "php_ajax/admin.php",
            type        : 'POST',
            contentType : false,
            processData : false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            async       : false,
            beforeSend : function() {
            },
            success : function(obj) {
                var formData = new FormData();
                formData.append("accion","cargar_usuario");
                formData.append("id_usuario",id_usuario);
                Misdatos.Cargar(formData,id_usuario);//estado agendado
                //Tutor.CargarPersonas();
                $('#tutoresModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    }
};