var id_clase, ClasesObj;
var Clases={
    Cargar:function(id_alumno,estado_id){
        var formData = new FormData();
        formData.append("accion","cargar_clases");
        formData.append("id_alumno",id_alumno);
        formData.append("estado_id",estado_id);
        var url="php_ajax/admin.php";
        $.ajax({
            url         : url,
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                if (obj.rst==1) {
                    HTMLCargarClases(obj.data);
                    ClasesObj=obj.data;
                }
                $('#tutoresModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    }
};