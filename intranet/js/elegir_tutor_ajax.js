var id_clase, ClasesObj;
var Clases={
    Cargar:function(id_alumno,estado_id){
        var formData = new FormData();
        formData.append("accion","cargar_clases_con_detalle");
        formData.append("id_alumno",id_alumno);
        formData.append("estado_id",estado_id);
        var url="php_ajax/admin.php";
        $.ajax({
            url         : url,
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                if (obj.rst==1) {
                    HTMLCargarClases(obj.data);
                    ClasesObj=obj.data;
                } else {
                    $("#tb_clases").html('');
                }
                $('#tutoresModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    },
    ElegirTutor:function(id_clase, id_tutor){
        var formData = new FormData();

        formData.append("accion","elegir_tutor");
        formData.append("id_clase",id_clase);
        formData.append("id_tutor",id_tutor);
        var url="php_ajax/admin.php";
        $.ajax({
            url         : url,
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                //Clases.Cargar(id_alumno,1);//estado agendado
                if (obj.rst==1) {
                    window.location ="/intranet/index.php?title=confirmadas";
                }
                $('#tutoresModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    },
    BuscarTutores:function(id_clase){
        var formData = new FormData();

        formData.append("accion","buscar_tutor");
        formData.append("id_clase",id_clase);
        var url="php_ajax/admin.php";
        $.ajax({
            url         : url,
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                BuscarTutoresHTML(obj);//estado agendado
                //Tutor.CargarPersonas();
                //$('#tutoresModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    },
};