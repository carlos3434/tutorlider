var alumno_id, AlumnoObj;
var Persona={
    AgregarEditarPersona:function(AE){
        var formData = new FormData();
        if(AE==1){
            formData.append("accion","editar_usuario");
            formData.append("id",AlumnoObj[alumno_id].id);
        } else {
            formData.append("accion","nuevo_usuario");
        }
        formData.append("rol_id",$('#form_alumnos #slct_rol_id').val());
        formData.append("nombres",$('#form_alumnos #txt_nombre').val());
        formData.append("apellido",$('#form_alumnos #txt_apellido').val());
        formData.append("correo",$('#form_alumnos #txt_email').val());
        formData.append("estado",$('#form_alumnos #slct_estado').val());
        formData.append("email_confirmado",$('#form_alumnos #slct_email_confirmado').val());
        formData.append("descripcion",$('#form_alumnos #txt_descripcion').val());
        formData.append("universidad",$('#form_alumnos #txt_universidad').val());
        formData.append("carrera",$('#form_alumnos #txt_carrera').val());
        formData.append("nivel",$('#form_alumnos #slct_nivel').val());
        formData.append("area",$('#form_alumnos #slct_area').val());
        formData.append("link",$('#form_alumnos #txt_link').val());
        formData.append("estado_link",$('#form_alumnos #slct_estado_link').val());
        var url="php_ajax/admin.php";
        $.ajax({
            url         : url,
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {

            },
            success : function(obj) {

                Persona.CargarPersonas();
                $('#personaModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    },
    CargarPersonas:function(){
        var formData = new FormData();
        formData.append("accion","cargar_usuario");
        //var datos=formData.serialize();
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                    HTMLCargarPersona(obj);
                    AlumnoObj=obj;
            },
            error: function(){
            }
        });
    },
    CargarRol:function(){
        var formData = new FormData();
        formData.append("accion","cargar_rol");
        //var datos=formData.serialize();
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                HTMLCargarRol(obj);
            },
            error: function(){
            }
        });
    },
    CambiarEstadoPersonas:function(id,AD){
        var formData = new FormData();
        formData.append("accion","cambiarEstado_usuario");
        formData.append("id",id);
        formData.append("estado",AD);
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                Persona.CargarPersonas();
                $('#personaModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });

    },
    ConsultarEstado:function(id,evento){
        var formData = new FormData();
        formData.append("accion","consultarEstado_usuario");
        formData.append("id",id);
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            async       : false,
            beforeSend : function() {

            },
            success : function(rst) {
                evento(rst);
            },
            error: function(){

            }
        });
    },
    AgendarClase:function(formData, evento){
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            async       : false,
            beforeSend : function() {

            },
            success : function(rst) {
                if (rst==2) {
                    alert("No tiene minutos suficientes");
                } else {
                    evento(rst);
                }
            },
            error: function(){

            }
        });
    },
    CargarClases:function(alumno_id){
        //getOpciones
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {alumno_id:alumno_id},
            async       : false,
            beforeSend : function() {

            },
            success : function(obj) {

            },
            error: function(){
            }
        });
    },
    CrearPlan:function(formData){
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            async       : false,
            beforeSend : function() {

            },
            success : function(obj) {
                if (obj.rst==1) {
                    window.location ="/intranet/index.php?title=misplanes";
                } else {
                    alert(obj.msj);
                }
            },
            error: function(){
            }
        });
    },
    AprobarVoucher:function(formData){

        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            async       : false,
            beforeSend : function() {

            },
            success : function(obj) {
                //$('.modal fade .modal-footer [data-dismiss="modal"]').click();
                if (obj.rst==1) {
                    $('#planesModal .modal-footer [data-dismiss="modal"]').click();
                    //window.location ="/intranet/index.php?title=misplanes";
                }
            },
            error: function(){
            }
        });
    }
};
