$(document).ready(function() {
    //
    var estado_id =1; //agendado
    Clases.Cargar(id_alumno,estado_id);//estado agenda

        //cargar modal para elegir tutor

    $('#tutoresModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // captura al boton
        //var titulo = button.data('titulo'); // extrae del atributo data-
        id = button.data('id'); //extrae el id del atributo data
        id_clase = button.data('id_clase'); //extrae el id del atributo data
        id_tutor = button.data('id_tutor'); //extrae el id del atributo data
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this); //captura el modal
       // modal.find('.modal-title').text(titulo+' Alumnos');
        $('#form_tutor [data-toggle="tooltip"]').css("display","none");
        $("#form_tutor input[type='hidden']").remove();
        //if(titulo=='Elegir'){
        if (id_tutor=='null' || id_tutor===null || id_tutor==='') {
            //si no tiene tutor cargar la lista de tutores que se matricularron a esta clase
            Clases.BuscarTutores(id_clase);
        } else {
            html='<section class="contenido-panel">';
            html+='<div class="contenido">';
            html+='<div class="col-md-5 col-sm-12 text-center">';
            html+='<img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">';
            html+='</div>';
            html+='<div class="col-md-7 col-sm-12 text-center">';
            html+='<h4>'+ClasesObj[id].nombres+' '+ClasesObj[id].apellido+'</h4>';
            html+='<p>'+ClasesObj[id].carrera+'</p>';
            html+='<p>'+ClasesObj[id].universidad+'</p>';
            html+='</div>';
            //html+='<div class="col-md-12 text-center">';
            //html+='<button type="button" class="btn btn-success elegir_tutor" data-id_tutor="'+ClasesObj[id_clase].id+'">Elegir</button>';
            //html+='</div>';
            html+='</div>';
            html+='</section>';
            $('#form_tutor').html(html);
        }


    });

    $('#tutoresModal').on('hide.bs.modal', function (event) {
        var modal = $(this); //captura el modal
        modal.find('.modal-body input').val(''); // busca un input para copiarle texto

    });
    
});
HTMLCargarClases=function(datos){
    var html="";
    $('#t_clases').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
        if(data.estado==1){
            estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
        }
        if (data.email_confirmado==1) {
            email_confirmado='SI';
        } else {
            email_confirmado='NO';
        }
        html+="<tr>"+
            "<td><i class='ico ion-bookmark'></i>  "+data.curso+" - "+data.tema+"</td>"+
            "<td><i class='ico ion-calendar'>  "+data.dia+' '+"</td>"+
            "<td><i class='ico ion-clock'></i>  "+data.hora+"</td>"+
            "<td><i class='ico ion-android-alarm-clock'>  "+data.tiempo+" min</td>"+
            //"<td id='estado_"+data.id+"' data-estado='"+data.estado+"'>"+estadohtml+"</td>"+
            '<td><a class="btn btn-success btn-sm" data-toggle="modal" data-target="#tutoresModal" data-id="'+index+'" data-id_clase="'+data.id+'" data-id_tutor="'+data.id_tutor+'"><i class="fa fa-edit fa-lg"></i> Elegir</a></td>';

        html+="</tr>";
    });
    $("#tb_clases").html(html);
    $("#t_clases").dataTable(); // inicializo el datatable
};
BuscarTutoresHTML=function(datos){
    var html="";
    if (datos=='no hay registros') {
          html+='<section class="contenido-panel">';
          html+='  <div class="contenido">';
          html+='    <div class="col-md-5 col-sm-12 text-center">';
          html+='      <img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">';
          html+='    </div>';
          html+='    <div class="col-md-7 col-sm-12 text-center">';
          html+='      <h4>Aun no contamos con tutores para esta clase</h4>';
          html+='    </div>';
          html+='  </div>';
          html+='</section>';
        $('#form_tutor').html(html);
    } else {
        $.each(datos,function(index,data){
            html+='<section class="contenido-panel">';
            html+='<div class="contenido">';
            html+='<div class="col-md-5 col-sm-12 text-center">';
            html+='<img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">';
            html+='</div>';
            html+='<div class="col-md-7 col-sm-12 text-center">';
            html+='<h4>'+data.nombres+' '+data.apellido+'</h4>';
            html+='<p>'+data.carrera+'</p>';
            html+='<p>'+data.universidad+'</p>';
            html+='</div>';
            html+='<div class="col-md-12 text-center">';
            html+='<button type="button" class="btn btn-success elegir_tutor"  data-id_tutor="'+data.id+'">Elegir</button>';
            html+='</div>';
            html+='</div>';
            html+='</section>';
        });
        $('#form_tutor').html(html);
        $('.elegir_tutor').click(function(event) {
            //enviar por ajax el tutor seleccionado
            //capturar clase
            //var id_clase=$(this).data('id_clase');
            var id_tutor=$(this).data('id_tutor');
            Clases.ElegirTutor(id_clase,id_tutor);
        });
    }
};