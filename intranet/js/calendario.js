var minutos;
var horas;

var logic = function( currentDateTime ){
  // 'this' is jquery object datetimepicker
  if ( currentDateTime !=null ) {
    minutos=currentDateTime.getMinutes();
    horas=currentDateTime.getHours();
  }
};

$('#datetimepicker').datetimepicker({
    timepicker:false,
    dayOfWeekStart : 1,
    lang:'es',
    format:'d-m-Y',
    minDate:'-1970/01/2',
});

$('#datetimepicker1').datetimepicker({
    datepicker:false,
    onChangeDateTime:logic,
    format:'g:i A',
    minTime:false,
    step:30,
});