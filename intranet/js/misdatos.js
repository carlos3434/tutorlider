var file;
$(document).ready(function() {
    $('.tutor').css('display','none');
    var formData = new FormData();
    formData.append("accion","cargar_usuario");
    formData.append("id_usuario",id_usuario);
    Misdatos.Cargar(formData,id_usuario);
    $('#foto').on('change', uploadFoto);
    $('#guardar').click(function(event) {
        /* Act on the event */
        var formData = new FormData();

        formData.append("accion","editar_usuario");
        formData.append("apellido",$('#txt_apellido').val() );
        formData.append("carrera",$('#txt_carrera').val() );
        formData.append("descripcion",$('#txt_descripcion').val() );
        formData.append("universidad",$('#txt_universidad').val() );
        formData.append("foto",file );
        formData.append("nombres",$('#txt_nombres').val() );
        formData.append("correo",$('#txt_correo').val() );
        formData.append("estado",$('#txt_estado').val() );
        formData.append("email_confirmado",$('#txt_email_confirmado').val() );
        formData.append("rol_id",$('#txt_rol_id').val() );
        if ($('#txt_contrasena').val() !=='') {
            formData.append("contrasena",$('#txt_contrasena').val());
        }
        formData.append("id",id_usuario);
        Misdatos.Editar(formData, id_usuario);
    });
});
uploadFoto=function(){
    files = event.target.files;
    event.stopPropagation();
    event.preventDefault();
    $.each(
        files, function(key, value)
        {
          file = value;
        }
    );
    var reader = new FileReader();

    reader.onload = function (e) {
        $('#img_foto').attr('src', e.target.result);
    };

    reader.readAsDataURL(files[0]);
};
HTMLCargarMisDatos=function(datos){
    $('#txt_apellido').val(datos.apellido);
    $('#txt_carrera').val(datos.carrera);
    $('#txt_descripcion').val(datos.descripcion);
    $('#txt_universidad').val(datos.universidad);
    $('#img_foto').attr('src', '/img/fotos/'+datos.foto);
    $('#txt_nombres').val(datos.nombres);
    //hidden
    $('#txt_correo').val(datos.email);
    $('#txt_estado').val(datos.estado);
    $('#txt_email_confirmado').val(datos.email_confirmado);
    $('#txt_rol_id').val(datos.rol_id);
    if (datos.rol_id==3) {
        $('.tutor').css('display','');
    }
};
BuscarTutoresHTML=function(datos){
    var html="";
    if (datos=='no hay registros') {
        //$('#form_tutor').html(html);
    } else {
        $.each(datos,function(index,data){
            html+='<section class="contenido-panel">';
            html+='<div class="contenido">';
            html+='<div class="col-md-5 col-sm-12 text-center">';
            html+='<img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">';
            html+='</div>';
            html+='<div class="col-md-7 col-sm-12 text-center">';
            html+='<h4>'+data.nombres+' '+data.apellido+'</h4>';
            html+='<p>'+data.carrera+'</p>';
            html+='<p>'+data.universidad+'</p>';
            html+='</div>';
            html+='<div class="col-md-12 text-center">';
            html+='<button type="button" class="btn btn-success elegir_tutor"  data-id_tutor="'+data.id+'">Elegir</button>';
            html+='</div>';
            html+='</div>';
            html+='</section>';
        });
        $('#form_tutor').html(html);
        $('.elegir_tutor').click(function(event) {
            //enviar por ajax el tutor seleccionado
            //capturar clase
            //var id_clase=$(this).data('id_clase');
            var id_tutor=$(this).data('id_tutor');
            Clases.ElegirTutor(id_clase,id_tutor);
        });
    }
};