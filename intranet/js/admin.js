$(document).ready(
    function() {
    Persona.CargarPersonas();
    Persona.CargarRol();
    slctGlobal.listarSlct('area','slct_area','multiple');
    slctGlobal.listarSlct('nivel','slct_nivel','multiple');

    $('#planesModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var titulo = button.data('titulo');
        alumno_id = button.data('id');
        Planes.Cargar(alumno_id);
    });
    $('#personaModal').on(
        'show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // captura al boton
        var titulo = button.data('titulo'); // extrae del atributo data-
        alumno_id = button.data('id'); //extrae el id del atributo data
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this); //captura el modal
        modal.find('.modal-title').text(titulo+' Usuario');
        $('#form_alumnos [data-toggle="tooltip"]').css("display","none");
        $("#form_alumnos input[type='hidden']").remove();
        if(titulo=='Nuevo'){

            modal.find('.modal-footer .btn-primary').text('Guardar');
            modal.find('.modal-footer .btn-primary').attr('onClick','Agregar();');
            $('#form_alumnos #slct_rol_id').val(3);
            $('#form_alumnos #slct_rol_id').removeAttr("disabled");
            $('#form_alumnos #slct_estado').val(1);
            $('.tutor').css('display','');
            $('.alumno').css('display','');
            $('#form_alumnos #txt_nombre').focus();
        } else{
            //Persona.CargarClases(AlumnoObj[alumno_id].id); //no es multiselect
            modal.find('.modal-footer .btn-primary').text('Actualizar');
            modal.find('.modal-footer .btn-primary').attr('onClick','Editar();');
            //AlumnoObj[alumno_id]
            $('#form_alumnos #txt_nombre').val(AlumnoObj[alumno_id].nombres);
            $('#form_alumnos #txt_apellido').val(AlumnoObj[alumno_id].apellido);
            //$('#form_alumnos #txt_password').val( '' );
            $('#form_alumnos #txt_email').val(AlumnoObj[alumno_id].email);
            $('#form_alumnos #slct_estado').val(AlumnoObj[alumno_id].estado);
            $('#form_alumnos #slct_rol_id').val(AlumnoObj[alumno_id].rol_id);
            $('#form_alumnos #slct_rol_id').prop('disabled', 'disabled');
            if (AlumnoObj[alumno_id].rol_id==3) {
                $('.tutor').css('display','');
                $('.alumno').css('display','none');
            } else if (AlumnoObj[alumno_id].rol_id==1) {
                $('.tutor').css('display','none');
                $('.alumno').css('display','');
            } else {
                $('.tutor').css('display','');
                $('.alumno').css('display','');
            }
            $('#form_alumnos #txt_descripcion').val(AlumnoObj[alumno_id].descripcion);
            $('#form_alumnos #txt_carrera').val(AlumnoObj[alumno_id].carrera);
            $('#form_alumnos #txt_universidad').val(AlumnoObj[alumno_id].universidad);
            $('#form_alumnos #slct_email_confirmado').val(AlumnoObj[alumno_id].email_confirmado);
            $('#form_alumnos #txt_link').val(AlumnoObj[alumno_id].link);
            $('#form_alumnos #slct_estado_link').val(AlumnoObj[alumno_id].estado_link);
            if (AlumnoObj[alumno_id].areas!=null) {
                var areas=AlumnoObj[alumno_id].areas.split(",");
                $('#form_alumnos #slct_area').val(areas);
                $('#slct_area').multiselect('refresh');
            }
            if (AlumnoObj[alumno_id].niveles!=null) {
                var niveles=AlumnoObj[alumno_id].niveles.split(",");
                $('#form_alumnos #slct_nivel').val(niveles);
                $('#slct_nivel').multiselect('refresh');
            }
            $("#form_alumnos").append("<input type='hidden' value='"+AlumnoObj[alumno_id].id+"' name='id'>");
        }
        $('#form_alumnos #slct_rol_id').change(
            function(event) {
                /* Act on the event */
                if ($(this).val()==3) {
                    $('.tutor').css('display','');
                    $('.alumno').css('display','none');
                }  else if (AlumnoObj[alumno_id].rol_id==1) {
                    $('.tutor').css('display','none');
                    $('.alumno').css('display','');
                } else {
                    $('.tutor').css('display','');
                    $('.alumno').css('display','');
                }
            }
        );

        }
    );

    $('#personaModal').on(
        'hide.bs.modal', function (event) {
            var modal = $(this); //captura el modal
            modal.find('.modal-body input').val(''); // busca un input para copiarle texto
            $('#slct_area').val('');
            $('#slct_area').multiselect('rebuild');
            $('#slct_area').multiselect('refresh');
            $('#slct_nivel').val('');
            $('#slct_nivel').multiselect('rebuild');
            $('#slct_nivel').multiselect('refresh');
        }
    );
    }
);
HTMLCargarRol=function(obj){
    var html="";
    $.each(
        obj,function(index,data){
        html += "<option value=\"" + data.id + "\">" + data.nombre + "</option>";

        }
    );
    $("#slct_rol_id").html(html);
};
Editar=function(){
    if(validaPersonas()){
        Persona.AgregarEditarPersona(1);
    }
};

activar=function(id){
    Persona.CambiarEstadoPersonas(id,1);
};
desactivar=function(id){
    Persona.CambiarEstadoPersonas(id,0);
};
aprobar_voucher=function(id,alumno_id, minutos){
    var precio_voucher=$('#precio_'+id).val();
    if (precio_voucher<0) {
        alert("debe ingresar un numero m,ayor a cero");
        return;
    }
    if (precio_voucher<0) {
        alert("debe ingresar un numero m,ayor a cero");
        return;
    }
    var formData = new FormData();
    formData.append("accion","aprobar_voucher");
    formData.append("id",id);
    formData.append("alumno_id",alumno_id);
    formData.append("minutos",minutos);
    formData.append("precio_voucher",precio_voucher);

    Persona.AprobarVoucher(formData);
};
Agregar=function(){
    if(validaPersonas()){
        Persona.AgregarEditarPersona(0);
    }
};
validaPersonas=function(){
    $('#form_alumnos [data-toggle="tooltip"]').css("display","none");
    var a=[];
    a[0]=valida("txt","nombre","");
    var rpta=true;

    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};

valida=function(inicial,id,v_default){
    var texto="Seleccione";
    if(inicial=="txt"){
        texto="Ingrese";
    }

    if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
        $('#error_'+id).attr('data-original-title',texto+' '+id);
        $('#error_'+id).css('display','');
        return false;
    }
};
HTMLCargarPlanes=function(datos){
    var html="",precio_voucher,aprobar;
    $('#t_planes').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        estadohtml='<span class="btn btn-'+data.alerta+'">'+data.estado_pago+'</span>';
        precio_voucher='no disponible';
        aprobar='no disponible';
        if (data.estado_pago_id==2) {
            precio_voucher="S./ <input type='number' name='precio_voucher' id='precio_"+data.id+"'>.00 ";
            aprobar='<span onClick="aprobar_voucher('+data.id+','+data.alumno_id+','+data.minutos+')" class="btn btn-success">Aprobar </span>';
        } else if (data.estado_pago_id==3 || data.estado_pago_id==4) {
            precio_voucher="S./ "+data.precio_voucher+".00 ";

        }

        html+="<tr>"+
            "<td>"+data.fecha_registro+"</td>"+
            "<td>"+data.minutos+' min. - '+data.nivel+"</td>"+
            "<td>"+data.medio_pago+"</td>"+
            "<td>"+estadohtml+"</td>"+
            "<td>S./ "+data.precio_registro+".00 </td>"+
            "<td>"+precio_voucher+"</td>"+
            "<td>"+aprobar+"</td>";
        html+="</tr>";
    });
    $("#tb_planes").html(html);
    $("#t_planes").dataTable();
};

HTMLCargarPersona=function(datos){
    var html="", voucher;
    $('#t_alumnos').dataTable().fnDestroy();

    $.each(
        datos,function(index,data){
        estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
        if(data.estado==1){
            estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
        }
        if (data.email_confirmado==1) {
            email_confirmado='SI';
        } else {
            email_confirmado='NO';
        }
        voucher="No disponible";
        if (data.rol_id==1 && data.estado==1) {
            voucher='<a class="btn btn-info btn-sm" data-toggle="modal" data-target="#planesModal" data-id="'+data.id+'" data-titulo="Ver"><i class="fa fa-edit fa-lg"></i> Ver</a>';
        }
        html+="<tr>"+
            "<td>"+data.apellido+"</td>"+
            "<td>"+data.nombres+' '+"</td>"+
            "<td>"+data.email+"</td>"+
            "<td>"+email_confirmado+"</td>"+
            "<td>"+data.rol+"</td>"+
            "<td>"+voucher+"</td>"+
            "<td>"+data.minutos+"</td>"+
            "<td id='estado_"+data.id+"' data-estado='"+data.estado+"'>"+estadohtml+"</td>"+
            '<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#personaModal" data-id="'+index+'" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> Editar</a></td>';

        html+="</tr>";
        }
    );
    $("#tb_alumnos").html(html);
    $("#t_alumnos").DataTable();
};
