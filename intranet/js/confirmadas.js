$(document).ready(function() {
    //
    var estado_id =2; //confirmadas
    Clases.Cargar(id_alumno,estado_id);//estado agenda

});
HTMLCargarClases=function(datos){
    var html="";
    $('#t_clases').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
        if(data.estado==1){
            estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
        }
        if (data.email_confirmado==1) {
            email_confirmado='SI';
        } else {
            email_confirmado='NO';
        }
        var foto =data.foto;
        if (foto==='') {
            foto='imagen/avatar.png';
        }
        var archivo_ruta =data.archivo_ruta;
        if (archivo_ruta!=='') {
            archivo_ruta="<a href='"+archivo_ruta+"' target='_blank'>Ver</a>";
        }
        var link =data.link;
        if (link!=='') {
            ruta_aula="<a href='"+link+"' target='_blank'>Ir</a>";
        } else {
            ruta_aula='Aula no habilitada';
        }
        html+="<tr>"+
            "<td><i class='ico ion-bookmark'></i>  "+data.curso+" - "+data.tema+"</td>"+
            "<td><i class='ico ion-calendar'>  "+data.dia+' '+"</td>"+
            "<td><i class='ico ion-clock'></i>  "+data.hora+"</td>"+
            "<td><i class='ico ion-android-alarm-clock'>  "+data.tiempo+" </td>"+
            "<td>"+data.apellido+" "+data.nombres+" "+data.universidad+" "+data.carrera+" </td>"+
            "<td> <img src='"+foto+"' class='img-circle' width='100px'> </td>"+
            "<td>"+archivo_ruta+" </td>"+
            "<td>"+ruta_aula+" </td>";
        html+="</tr>";
    });
    $("#tb_clases").html(html);
    $("#t_clases").dataTable(); // inicializo el datatable
};