$(document).ready(function() {
    //
    var estado_id =1; //agendado
    Clases.Cargar(id_alumno,estado_id);//estado agenda

        //cargar modal para elegir tutor

    $('#tutoresModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // captura al boton
        //var titulo = button.data('titulo'); // extrae del atributo data-
        id = button.data('id'); //extrae el id del atributo data
        id_clase = button.data('id_clase'); //extrae el id del atributo data
        id_tutor = button.data('id_tutor'); //extrae el id del atributo data
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this); //captura el modal
       // modal.find('.modal-title').text(titulo+' Alumnos');
        $('#form_tutor [data-toggle="tooltip"]').css("display","none");
        $("#form_tutor input[type='hidden']").remove();
        //if(titulo=='Elegir'){
        if (id_tutor=='null' || id_tutor===null || id_tutor==='') {
            //si no tiene tutor cargar la lista de tutores que se matricularron a esta clase
            Clases.BuscarTutores(id_clase);
        } else {
            html='<section class="contenido-panel">';
            html+='<div class="contenido">';
            html+='<div class="col-md-5 col-sm-12 text-center">';
            html+='<img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">';
            html+='</div>';
            html+='<div class="col-md-7 col-sm-12 text-center">';
            html+='<h4>'+ClasesObj[id].nombres+' '+ClasesObj[id].apellido+'</h4>';
            html+='<p>'+ClasesObj[id].carrera+'</p>';
            html+='<p>'+ClasesObj[id].universidad+'</p>';
            html+='</div>';
            //html+='<div class="col-md-12 text-center">';
            //html+='<button type="button" class="btn btn-success elegir_tutor" data-id_tutor="'+ClasesObj[id_clase].id+'">Elegir</button>';
            //html+='</div>';
            html+='</div>';
            html+='</section>';
            $('#form_tutor').html(html);
        }


    });

    $('#tutoresModal').on('hide.bs.modal', function (event) {
        var modal = $(this); //captura el modal
        modal.find('.modal-body input').val(''); // busca un input para copiarle texto

    });
    
});
HTMLCargarClases=function(datos){
    var html="";
    //$('#t_clases').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
        if(data.estado==1){
            estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
        }
        if (data.email_confirmado==1) {
            email_confirmado='SI';
        } else {
            email_confirmado='NO';
        }
        /*
        html+="<tr>"+
            "<td><i class='ico ion-bookmark'></i>  "+data.curso+" - "+data.tema+"</td>"+
            "<td><i class='ico ion-calendar'>  "+data.dia+' '+"</td>"+
            "<td><i class='ico ion-clock'></i>  "+data.hora+"</td>"+
            "<td><i class='ico ion-android-alarm-clock'>  "+data.tiempo+" min</td>"+
            //"<td id='estado_"+data.id+"' data-estado='"+data.estado+"'>"+estadohtml+"</td>"+
            '<td><a class="btn btn-success btn-sm" data-toggle="modal" data-target="#tutoresModal" data-id="'+index+'" data-id_clase="'+data.id+'" data-id_tutor="'+data.id_tutor+'"><i class="fa fa-edit fa-lg"></i> Elegir</a></td>';
        html+="</tr>";
        */
       // var i=i+index;
        var id='heading'+index;
        var id2='collapse'+index;

        if (index=='0') {
            expanded="true";
            clas ='in';
        } else {
            clas ='';
            expanded="false";
        }
        html2='<form id="form_tutor" name="form_tutor" action="" method="post">'+
                ''+
                '  <section class="contenido-panel">'+
                '    <div class="contenido">'+
                '      <div class="col-md-5 col-sm-12 text-center">'+
                '        <img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">'+
                '      </div>'+
                '      <div class="col-md-7 col-sm-12 text-center">'+
                '        <h4>Aun no contamos con tutores para esta clase</h4>'+
                '      </div>'+
                '    </div>'+
                '  </section>'+
                '  '+
                '</form>';
         if (data.tutores!=='') {
             tutores = data.tutores.split('|');
             if (tutores.length>0) {
                html2='';
                 for (var j = 0; j < tutores.length; j++) {
                     tutor=tutores[j].split('-');
                     html2+='<section class="contenido-panel">';
                    html2+='<div class="contenido">';
                    html2+='<div class="col-md-5 col-sm-12 text-center">';
                    if (tutor[5]==='') {
                        foto='imagen/avatar.png';
                    } else {
                        foto=tutor[5];
                    }
                    html2+='<img id="foto" src="'+foto+'" class="img-circle" width="100px">';
                    html2+='</div>';
                    html2+='<div class="col-md-7 col-sm-12 text-center">';
                    html2+='<h4>'+tutor[1]+' '+tutor[2]+'</h4>';
                    html2+='<p>'+tutor[3]+'</p>';
                    html2+='<p>'+tutor[4]+'</p>';
                    html2+='</div>';
                    html2+='<div class="col-md-12 text-center">';

                    html2+='<button type="button" class="btn btn-success elegir_tutor" data-id_clase="'+data.id+'" data-id_tutor="'+tutor[0]+'">Elegir</button>';
                    html2+='</div>';
                    html2+='</div>';
                    html2+='</section>';

                 }

             }
         }
        html+='<div class="panel panel-default">'+
             '  <div class="panel-heading" role="tab" id="'+id+'">'+
             '    <h4 class="panel-title">'+
             '      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#'+id2+'" aria-expanded="'+expanded+'" aria-controls="'+id2+'">'+
             //'        #'+i+
            //aca deberia ir la cabezera
            //data.id es la id_clase
                    ""+data.curso+" - "+data.tema+""+
                    "            "+data.dia+'    '+
                    "            "+data.hora+"    "+
                    "            "+data.tiempo+"    "+

             '      </a>'+
            '    </h4>'+
             '  </div>'+
             '  <div id="'+id2+'" class="panel-collapse collapse '+clas+'" role="tabpanel" aria-labelledby="'+id+'">'+
             '    <div class="panel-body">';

        html+=html2;
             html+='    </div>'+
             '  </div>'+
             '</div>';
    });
    $("#t_clases").html(html);
    //$("#t_clases").dataTable(); // inicializo el datatable
    $('.elegir_tutor').click(function(event) {
        //enviar por ajax el tutor seleccionado
        //capturar clase
        var id_clase=$(this).data('id_clase');
        var id_tutor=$(this).data('id_tutor');
        Clases.ElegirTutor(id_clase,id_tutor);
    });
};
BuscarTutoresHTML=function(datos){
    var html="";
    if (datos=='no hay registros') {
        //$('#form_tutor').html(html);
    } else {
        $.each(datos,function(index,data){
            html+='<section class="contenido-panel">';
            html+='<div class="contenido">';
            html+='<div class="col-md-5 col-sm-12 text-center">';
            html+='<img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">';
            html+='</div>';
            html+='<div class="col-md-7 col-sm-12 text-center">';
            html+='<h4>'+data.nombres+' '+data.apellido+'</h4>';
            html+='<p>'+data.carrera+'</p>';
            html+='<p>'+data.universidad+'</p>';
            html+='</div>';
            html+='<div class="col-md-12 text-center">';
            html+='<button type="button" class="btn btn-success elegir_tutor"  data-id_tutor="'+data.id+'">Elegir</button>';
            html+='</div>';
            html+='</div>';
            html+='</section>';
        });
        $('#form_tutor').html(html);
        $('.elegir_tutor').click(function(event) {
            //enviar por ajax el tutor seleccionado
            //capturar clase
            //var id_clase=$(this).data('id_clase');
            var id_tutor=$(this).data('id_tutor');
            Clases.ElegirTutor(id_clase,id_tutor);
        });
    }
};