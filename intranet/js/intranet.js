var file, precio_contrata, nivel, estadoAlumno;
$(document).ready(function() {
  $('#exampleInputFile').on('change', uploadFile);
  Persona.ConsultarEstado(id, cargarModal);
});
agendar=function() {
  //vlidar si es usuario con estado 1
  Persona.ConsultarEstado(id, registrarAgenda);
};
adquirir=function() {
  if (estadoAlumno===undefined || estadoAlumno=='0' ) {
     $('#myModaladquirir').modal('show');
  }
};
uploadFile=function(){
    files = event.target.files;
    event.stopPropagation();
    event.preventDefault();
    $.each(
        files, function(key, value)
        {
          file = value;
        }
    );
};
registrarAgenda=function(rst) {
  if (rst.estado=='0') {
   $('#myModaladquirir').modal('show');
  } else {
    //continuar con agenda
    var formData = new FormData();
        formData.append("accion","agendar_clase");
        formData.append("id",id);
    if ( $('#curso').val()!=='' &&
          $('#tema').val()!=='' &&
          $('input[name=nivel]:checked')!=='' &&
          $('#datetimepicker').val()!=='' &&
          $('#datetimepicker1').val()!==''
        ) {

        formData.append("curso",$('#curso').val());
        formData.append("tema",$('#tema').val());
        formData.append("area", $('input[name=area]:checked').val());
        formData.append("nivel", $('input[name=nivel]:checked').val());

        formData.append("tiempo", $('#tiempo').val());
        formData.append("fecha", $('#datetimepicker').val());
        formData.append("hora", $('#datetimepicker1').val());
        formData.append('archivo', file);
        formData.append('horas', horas);
        formData.append('minutos', minutos);
      Persona.AgendarClase(formData, redirige);
    }
  }
};
redirige=function(rst){
  if (rst==1) {
    window.location ="/intranet/index.php?title=agendadas";
  }
};
cargarModal=function(rst){
  estadoAlumno=rst.estado;
  if (rst===undefined || rst.estado=='0' ) {
     $('#myModaladquirir').modal('show');
  }
};
metodo=function(val) {
    if (val == "bcp") {
      $("#myModal2").modal('hide');
      $("#myModal3").modal('show');
    }
    else {
      $("#myModal2").modal('hide');
      $("#myModal4").modal('show');
    }
};
elegir=function() {
  $("#myModal3").modal('hide');
  $("#myModal4").modal('hide');
  $("#myModal2").modal('show');
};
contratar=function(numero) {
    //var numero = numero;
    var tipo_cambio = 3;
    switch (numero) {
      case 'cole_30':
          nivel ='2';
          precio_contrata = 17;
          tipo_plan='1';
          descripcion = "Resuelve una duda (30 minutos)";
        break;
      case 'uni_30':
          nivel ='1';
          precio_contrata = 20;
          tipo_plan='1';
          descripcion = "Resuelve una duda (30 minutos)";
        break;
      case 'cole_60':
          nivel ='2';
          precio_contrata = 30;
          tipo_plan='2';
          descripcion = "Ver un tema (1 hora)";
        break;
      case 'uni_60':
          nivel ='1';
          precio_contrata = 35;
          tipo_plan='2';
          descripcion = "Ver un tema (1 hora)";
        break;
      case 'cole_240':
          nivel ='2';
          precio_contrata = 114;
          tipo_plan='3';
          descripcion = "Aprueba un examen (4 hora)";
        break;
      case 'uni_240':
          nivel ='1';
          precio_contrata = 134;
          tipo_plan='3';
          descripcion = "Aprueba un examen (4 hora)";
        break;
      case 'cole_720':
          nivel ='2';
          precio_contrata = 324;
          tipo_plan='4';
          descripcion = "El mejor de la clase (12 hora)";
        break;
      case 'uni_720':
          nivel ='1';
          precio_contrata = 384;
          tipo_plan='4';
          descripcion = "El mejor de la clase (12 hora)";
        break;

      default:

        break;
    }
    precio_contrata_dolar = precio_contrata/tipo_cambio;
    $('#precio_contrata').html(precio_contrata);
    $('#precio_contrata1').html(precio_contrata);
    $('#precio_contrata2').html(precio_contrata);
    $('#item_name').val(descripcion);
    $('#amount').val(precio_contrata_dolar.toFixed(2));
    $('#myModaladquirir').modal('hide');
    $('#myModal2_1').modal('show');
    $('.tipo_plan').html(tipo_plan);
};
crear_plan=function( medio_pago_id){
  tipo_plan = $('.tipo_plan').html();
  var formData = new FormData();
  formData.append("accion","crear_plan");
  formData.append("tipo_plan",tipo_plan);
  formData.append("medio_pago_id",medio_pago_id);
  formData.append("nivel",nivel);
  formData.append("precio_contrata",precio_contrata);
  Persona.CrearPlan(formData);
};