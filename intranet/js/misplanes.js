var file;
$(document).ready(function() {
    Planes.Cargar(id_alumno);

    $('#misplanesModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget);
        id = button.data('id');
        id_plan = button.data('id_plan');
        var modal = $(this);

        $('#form_misplanes [data-toggle="tooltip"]').css("display","none");
        if (PlanesObj[id].voucher===null || PlanesObj[id].voucher ==='null') {
            $('.upload').css('display','');
        } else {
            $('.upload').css('display','none');
            $('#form_misplanes #voucher').attr('src','../img/voucher/'+ PlanesObj[id].voucher);
        }
        $("#form_misplanes input[type='hidden']").remove();
        $("#upload").on('change',function() {
            CargarImagen(this);
        });
        $('#upload').on('change', uploadFile);
    });

    $('#misplanesModal').on('hide.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body input').val('');
        $('#form_misplanes #voucher').attr('src', '');
        file=undefined;
    });
    
});
subir_voucher=function(){
    if (file!==undefined && file!=='undefined') {
        var formData = new FormData();
        formData.append('accion', 'subir_voucher');
        formData.append('id_plan', id_plan);
        formData.append('voucher', file);
        Planes.SubirVoucher(formData);
    }
};
uploadFile=function(){
    files = event.target.files;
    event.stopPropagation();
    event.preventDefault();
    $.each(
        files, function(key, value)
        {
          file = value;
        }
    );
};
CargarImagen=function(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#voucher').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};
HTMLCargarPlanes=function(datos){
    var html="";
    $('#t_planes').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        estadohtml='<span class="btn btn-'+data.alerta+'">'+data.estado_pago+'</span>';

        html+="<tr>"+
            "<td>"+data.fecha_registro+"</td>"+
            "<td>"+data.minutos+' min. - '+data.nivel+"</td>"+
            "<td>"+data.medio_pago+"</td>"+
            "<td>"+estadohtml+"</td>"+
            "<td>S./ "+data.precio_registro+".00 </td>"+
            '<td><a class="btn btn-success btn-sm" data-toggle="modal" data-target="#misplanesModal" data-id="'+index+'" data-id_plan="'+data.id+'" ><i class="fa fa-edit fa-lg"></i> Elegir</a></td>';

        html+="</tr>";
    });
    $("#tb_planes").html(html);
    $("#t_planes").dataTable();
};
