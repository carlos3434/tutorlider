var slctGlobal={
    listarSlct:function(elemento,slct,tipo,valarray,data,afectado,afectados,slct_id,slctant,slctant_id){
        $.ajax({
            url         : 'php_ajax/listar.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {elemento:elemento},
            beforeSend : function() {
            },
            success : function(obj) {
                if(obj.rst==1){
                    htmlListarSlct(obj,slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id);
                }
            },
            error: function(){
            }
        });
    },
    listarSlct2:function(elemento,slct,data){
        $.ajax({
            url         : elemento+'/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
            },
            success : function(obj) {
                if(obj.rst==1){
                    html='<option value="">Seleccione</option>';
                    $.each(obj.datos,function(index,data){
                            html += "<option value=\"" + data.id + "\" >" + data.nombre + "</option>";
                    });
                    $("#"+slct).html(html);
                }
            },
            error: function(){
            }
        });
    }
};
