var id_plan, PlanesObj;
var Planes={
    Cargar:function(id_alumno){
        var formData = new FormData();
        formData.append("accion","cargar_planes");
        formData.append("id_alumno",id_alumno);
        var url="php_ajax/admin.php";
        $.ajax({
            url         : url,
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            beforeSend : function() {
            },
            success : function(obj) {
                if (obj.rst==1) {
                    HTMLCargarPlanes(obj.data);
                    PlanesObj=obj.data;
                } else {
                    $("#tb_planes").html('');
                }
                //$('#misplanesModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){
            }
        });
    },
    SubirVoucher:function(formData){
        $.ajax({
            url         : 'php_ajax/admin.php',
            type        : 'POST',
            contentType: false,
            processData: false,
            cache       : false,
            dataType    : 'json',
            data        : formData,
            async       : false,
            beforeSend : function() {

            },
            success : function(rst) {
                Planes.Cargar(id_alumno);
                $('#misplanesModal .modal-footer [data-dismiss="modal"]').click();
            },
            error: function(){

            }
        });
    },
};