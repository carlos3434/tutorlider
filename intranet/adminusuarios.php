<div class="blockcontenido">
  <section class="contenido">
    <span class="encabezado"><a href="index.php">Home /</a> Administrar usuarios</span>
  </section>
  <section class="contenido">

        <div class="row">
            <div class="col-xs-12">
                <!-- Inicia contenido -->
                <div class="box">
                    <div class="box-body table-responsive">
                        <a class='btn btn-primary btn-sm' class="btn btn-primary" data-toggle="modal" data-target="#personaModal" data-titulo="Nuevo">
                          <i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo
                        </a>
                        <table id="t_alumnos" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Apellidos</th>
                                    <th>Nombres</th>
                                    <th>Email</th>
                                    <th>Confirmo Email</th>
                                    <th>Rol</th>
                                    <th>Planes</th>
                                    <th>Minutos</th>
                                    <th>Estado</th>
                                    <th> [ ] </th>
                                </tr>
                            </thead>
                            <tbody id="tb_alumnos"></tbody>
                            <tfoot>
                                <tr>
                                    <th>Apellidos</th>
                                    <th>Nombres</th>
                                    <th>Email</th>
                                    <th>Confirmo Email</th>
                                    <th>Rol</th>
                                    <th>Planes</th>
                                    <th>Minutos</th>
                                    <th>Estado</th>
                                    <th> [ ] </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <!-- Finaliza contenido -->
            </div>
        </div>

  </section><!-- /.content -->
  <?php include('form/admin_modal.php'); ?>
  <?php include('form/admin_planes_modal.php'); ?>
  <div class="row">
    <div class="col-lg-12">
      <section class="contenido">
        <p>Copyright 2015 © <strong>Tutor Líder</strong></p>
      </section>
    </div>
  </div>
</div>
  <link rel="stylesheet" href='js/multiselect/bootstrap-multiselect.css'>
  <script src='js/multiselect/bootstrap-multiselect.js'></script>
  <script src='js/multiselect/slct_global_ajax.js'></script>
  <script src='js/multiselect/slct_global.js'></script>
<script type="text/javascript" src="js/admin_ajax.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript" src="js/misplanes_ajax.js"></script>
