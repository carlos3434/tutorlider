<div class="blockcontenido">
  <section class="contenido">
    <span class="encabezado"><a>Home</a></span>
  </section>
  <?php if ($_SESSION['email_confirmado'] == 0) : ?>
  <section class="contenido-panel">
    <div class="alert alert-danger" role="alert">
      <strong>Opss..</strong> Tu correo electrónico esta sin verificar, reviza tu bandeja de entrada o correos no deseados (Dar este correo es seguro).
    </div>
    <div class="alert alert-danger" role="alert">
      Si no has recibido ningun correo has <a href="#" class="alert-link">click aquí</a> o escribenos a alertas@tutorlider.com
    </div>
  </section>
  <?php
  endif;
  ?>
  <?php if ($_SESSION['rol_id'] == 1) : //alumno?>
  <section class="contenido-panel">
    <div class="row">
      <div class="col-lg-3 col-sm-6 cajas">
        <div class="panel-verde">
          <div class="row">
            <div class="col-xs-3">
              <span class="panel-icono"><i class="ico ion-thumbsup"></i></span>
            </div>
            <div class="col-xs-9">
              <span class="panel-numero"><?php echo $agendadas ?></span>
              <p>Clases agendadas</p>
            </div>
            <div class="col-xs-12">
              <div class="sub text-center">
                <a href="index.php?title=agendadas">más info <i class="ico ion-log-out"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 cajas">
        <div class="panel-celeste">
          <div class="row">
            <div class="col-xs-3">
              <span class="panel-icono"><i class="ico ion-load-a"></i></span>
            </div>
            <div class="col-xs-9">
              <span class="panel-numero"><?php echo $agendadas ?></span>
              <p>Elige un tutor</p>
            </div>
            <div class="col-xs-12">
              <div class="sub text-center">
                <a href="index.php?title=agendadas">más info <i class="ico ion-log-out"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 cajas">
        <div class="panel-rojo">
          <div class="row">
            <div class="col-xs-3">
              <span class="panel-icono"><i class="ico ion-checkmark"></i></span>
            </div>
            <div class="col-xs-9">
              <span class="panel-numero"><?php echo $confirmadas ?></span>
              <p>Clases confirmadas</p>
            </div>
            <div class="col-xs-12">
              <div class="sub text-center">
                <a href="index.php?title=confirmadas">más info <i class="ico ion-log-out"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 cajas">
        <div class="panel-naranja">
          <div class="row">
            <div class="col-xs-3">
              <span class="panel-icono"><i class="ico ion-university"></i></span>
            </div>
            <div class="col-xs-9">
              <span class="panel-numero"></span>
              <p>Mis planes</p>
            </div>
            <div class="col-xs-12">
              <div class="sub text-center">
                <a href="index.php?title=misplanes">más info <i class="ico ion-log-out"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="contenido-panel">
    <div class="row">
      <div class="col-md-7">
        <section class="contenido">
          <p class="encabezado text-center">Agendar Clases</p>
          <form method="post" action="" enctype="multipart/form-data">
            <div class="form-group">
              <label for="curso">Area</label>
              <div class="form-group col-md-12">
                <label class="radio-inline">
                  <input onclick="adquirir()" type="radio" name="area" id="area1" value="1"> Matemática
                </label>
                <label class="radio-inline">
                  <input onclick="adquirir()" type="radio" name="area" id="area2" value="2"> Física
                </label>
                <label class="radio-inline">
                  <input onclick="adquirir()" type="radio" name="area" id="area3" value="3"> Química
                </label>
              </div>
            </div>
            <div class="form-group">
              <label for="curso">Curso</label>
              <input onclick="adquirir()" type="text" class="form-control" id="curso" name="curso" placeholder="Ejemplo: Química" required>
            </div>
            <div class="form-group">
              <label for="tema">Tema o duda</label>
              <input onclick="adquirir()" type="text" class="form-control" id="tema" name="tema" placeholder="Ejemplo: Estructura atómica" required>
            </div>
            <div class="form-group">
              <label for="curso">Nivel</label>
              <div class="form-group col-md-12">
                <label class="radio-inline">
                <input onclick="adquirir()" type="radio" name="nivel" id="nivel1" value="1"> Universidad
              </label>
              <label class="radio-inline">
                <input onclick="adquirir()" type="radio" name="nivel" id="nivel2" value="2"> Colegio
              </label>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Sube un Archivo</label>
              <input onclick="adquirir()" type="file" id="exampleInputFile" name="archivo">
              <p class="help-block">Ejemplo: Copias del profesor</p>
            </div>
            <div class="form-group">
              <label for="tiempo">¿Cuánto tiempo deseas estudiar?</label>
              <select onclick="adquirir()" name="tiempo" id="tiempo" type="text" class="form-control" placeholder="Escoga el Timepo">
                <option value="30">30 minutos</option>
                <option value="60">1 hora</option>
                <option value="90">1 hora 30 minutos</option>
                <option value="120">2 horas</option>
                <option value="150">2 horas 30 minutos</option>
                <option value="180">3 horas</option>
              </select>
            </div>
            <div class="form-group">
              <label for="fecha">¿Cuando deseas estudiar?</label>
              <div class="row">
                <div class="col-lg-6">
                  <input onclick="adquirir()" type="text" name="fecha" class="form-control text-center" placeholder="Fecha" id="datetimepicker" onfocus="blur()">
                </div>
                <div class="col-lg-6">
                  <input onclick="adquirir()" type="text" name="hora" class="form-control text-center" placeholder="hora" id="datetimepicker1" onfocus="blur()">
                </div>
              </div>
            </div>
            <div class="form-group text-center">
              <a href="#" onclick="agendar()" class="text-center btn btn-primary">Agendar</a>
            </div>
          </form>
        </section>
      </div>
      <div class="col-md-5">
        <section class="contenido">
          <p class="encabezado text-center">Avisos</p>
          <table class="table">
            <tr>
              <td <i class="ico ion-alert-circled"></i></td>
              <td>Antes de programar una clase necesita adquirir un plan, adquieralo desde <a onclick="cargarModal()" href="#">AQUÍ</a> <span class="label label-danger">Importante</span></td>
            </tr>
            <tr>
              <td><i class="ico ion-android-mail"></i></td>
              <td>Si tiene algo que comentarnos algun error o problema escribenos a contacto@tutorlider.com o sin salir de la plataforma desde el siguiente enlace <a href="#">AQUÍ</a> <span class="label label-warning">Informativo</span></td>
            </tr>
           <!--<tr>
              <td><i class="ico ion-alert-circled"></i> Tienes tutores por seleccion para 3 clases <span class="label label-danger">Importante</span></td>
              <td><a href="#">Ver</a></td>
            </tr>
            <tr>
              <td><i class="ico ion-clock"></i> Tus clases comienza en 10 min <span class="label label-danger">Importante</span></td>
              <td><a href="#">Ver</a></td>
            </tr>
            <tr>
              <td><i class="ico ion-android-mail"></i> Tienes 4 mensajes por leer <span class="label label-warning">Informativo</span></td>
              <td><a href="#">Ver</a></td>
            </tr>-->
          </table>
        </section>
      </div>
    </div>
    </section>
  <?php endif ?>
  <div class="row">
    <div class="col-lg-12">
      <section class="contenido">
        <p>Copyright 2015 © <strong>Tutor Líder</strong></p>
      </section>
    </div>
  </div>
</div>
<!-- compra de paquetes-->
<!-- Modal -->
<div class="modal fade" id="myModal2_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title text-center" id="myModalLabel">Seleccione el método de pago</h3>
      </div>
      <div class="modal-body">
        <h2 class="text-center">S/. <span id="precio_contrata"></span><span style="font-size:25px">.00<span></h2>
        <br>
        <div class="text-center">
          <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input name="cmd" type="hidden" value="_xclick" />
            <input name="business" type="hidden" value="marcelino.vilcahuaman@outlook.com" />
            <input name="shopping_url" type="hidden" value="ladirecciondetuweb" />
            <input name="lc" type="hidden" value="ES" />
            <input name="notify_url" type="hidden" value="http://tutorlider/intranet/index.php" />
            <input name="item_name" type="hidden" id="item_name" value="" />
            <input name="amount" id="amount" type="hidden" value="" />
            <input type="image" src="imagen/paypal.png" width="150px" name=""submit/>
          </form>
          <h5><strong>* Tipo de cambio S/. 3.00</strong></h5>
        </div>
        <br>
        <div class="text-center">
          <input type="image" src="imagen/bcp.png" width="150px" onclick="metodo('bcp')" submit/>
          <h5><strong>* Solo Lima</strong></h5>
        </div>
        <br>
        <div class="text-center">
          <input type="image" src="imagen/interbank.png" width="150px" onclick="metodo('interbank')" submit/>
          <h5><strong>* Lima y Provincias</strong></h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 2 BCP -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title text-center" id="myModalLabel">BCP</h3>
      </div>
      <div class="modal-body">
        <div class="tipo_plan" style="display:none;"></div>
        <h2 class="text-center">S/. <span id="precio_contrata1"></span><span style="font-size:25px">.00<span></h2>
        <br>
        <div class="text-center">
          <img src="imagen/bcp.png" width="250px">
          <h5><strong>* Solo Lima</strong></h5>
        </div>
        <div class="row">
          <div class="col-md-2">
          </div>
          <div class="col-md-8">
            <p><strong>Número de Cuenta :</strong> 192 317 82 199 0 29 </p>
            <p><strong>Titular de la Cuenta:</strong> José Luis </p>
          </div>
          <div class="col-md-2">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-1">
          </div>
          <div class="col-md-10">
            <p>Una vez realizado el depósito por favor enviar una imagen del voucher a <strong>pagos@tutorlider.com</strong> o subir la imagen desde nuestra plataforma en el menu pagos.</p>
            <p>Una vez confirmado su pago su clase se encontrará disponible</p>
            <p>Gracias.</p>
          </div>
          <div class="col-md-1">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="crear_plan(1)">Aceptar</button>
        <button type="button" class="btn btn-danger" onclick="elegir()">Volver a seleccionar</button>
      </div>
    </div>
  </div>
</div>
<!--Modal interbank-->
<!-- Modal 3-->
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title text-center" id="myModalLabel">INTERBANK</h3>
      </div>
      <div class="modal-body">
        <div class="tipo_plan" style="display:none;"></div>
        <h2 class="text-center">S/. <span id="precio_contrata2"></span><span style="font-size:25px">.00<span></h2>
        <br>
        <div class="text-center">
          <img src="imagen/interbank.png" width="250px">
          <h5><strong>* Lima y Provincias</strong></h5>
        </div>
        <div class="row">
          <div class="col-md-2">
          </div>
          <div class="col-md-8">
            <p><strong>Número de Cuenta :</strong> 299 - 3075 - 244 - 284</p>
            <p><strong>Titular de la Cuenta:</strong> Bernabé Marcelino </p>
          </div>
          <div class="col-md-2">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-1">
          </div>
          <div class="col-md-10">
            <p>Una vez realizado el depósito por favor enviar una imagen del voucher a <strong>pagos@tutorlider.com</strong> o subir la imagen desde nuestra plataforma en el menu pagos.</p>
            <p>Una vez confirmado su pago su clase se encontrará disponible</p>
            <p>Gracias.</p>
          </div>
          <div class="col-md-1">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="crear_plan(2)">Aceptar</button>
        <button type="button" class="btn btn-danger" onclick="elegir()">Volver a seleccionar</button>
      </div>
    </div>
  </div>
</div><!--Modal 3-->
<!-- End de compra de paquetes-->

<!-- Modal Adquirir -->
  <div class="modal fade" id="myModaladquirir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title text-center" id="myModalLabel">Adquiera un plan</h3>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <h5><strong>Antes de programar una clase, necesita adquirir un plan</strong></h5>
          </div>
          <!-- Beneficios -->
          <div class="row">
            <div class="col-sm-6 col-xs-12 paquete">
              <div class="servicio">
                <div class="servicio_head">
                  <p>Resuelve una duda</p>
                  <p class="minutos"><strong>30 minutos</strong></p>
                </div>
                <div class="servicio_body">
                  <p class="text-center">Olvídate de esas dudas puntuales que te hacen confundir</p>
                </div>
                <div class="servicio_footer">
                  <div class="col-xs-6 colegio">
                    <p>Colegio</p>
                    <p class="precio">S/. 17</p>
                    <button onclick="contratar('cole_30')" type="button" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                  <div class="col-xs-6">
                    <p>Universidad</p>
                    <p class="precio">S/. 20</p>
                    <button onclick="contratar('uni_30')" type="button" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 paquete">
              <div class="servicio">
                <div class="servicio_head">
                  <p>Ver un tema</p>
                  <p class="minutos"><strong>1 hora</strong></p>
                </div>
                <div class="servicio_body">
                  <p class="text-center">¿Quieres repasar,aprender o dominar un tema?.Te sugerimos este plan</p>
                </div>
                <div class="servicio_footer">
                  <div class="col-xs-6 colegio">
                    <p>Colegio</p>
                    <p class="precio">S/. 30</p>
                    <button type="button" class="btn btn-nuevo">Ahorra S/. 4</button>
                    <br>
                    <button type="button" onclick="contratar('cole_60')" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                  <div class="col-xs-6">
                    <p>Universidad</p>
                    <p class="precio">S/. 35</p>
                    <button type="button" class="btn btn-nuevo">Ahorra S/. 5</button>
                    <br>
                    <button type="button" onclick="contratar('uni_60')" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 paquete">
              <div class="servicio">
                <div class="servicio_head">
                  <p>Aprueba un Examen</p>
                  <p class="minutos"><strong>4 horas</strong></p>
                </div>
                <div class="servicio_body">
                  <p class="text-center">Tienes un examen urgente y no sabes por donde empezar.Este plan es la solución</p>
                </div>
                <div class="servicio_footer">
                  <div class="col-xs-6 colegio">
                    <p>Colegio</p>
                    <p class="precio">S/. 114</p>
                    <button type="button" class="btn btn-nuevo">Ahorra S/. 22</button>
                    <br>
                    <button type="button" onclick="contratar('cole_240')" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                  <div class="col-xs-6">
                    <p>Universidad</p>
                    <p class="precio">S/. 134</p>
                    <button type="button" class="btn btn-nuevo">Ahorra S/. 26</button>
                    <br>
                    <button type="button" onclick="contratar('uni_240')" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 paquete">
              <div class="servicio">
                <div class="servicio_head">
                  <p>El mejor de la CLase</p>
                  <p class="minutos"><strong>12 horas</strong></p>
                </div>
                <div class="servicio_body">
                  <p class="text-center">Apoyo continuo con plan de estudio,reportes de avance,guías y mucho más.Atrévete a ser el mejor</p>
                </div>
                <div class="servicio_footer">
                  <div class="col-xs-6 colegio">
                    <p>Colegio</p>
                    <p class="precio">S/. 324</p>
                    <button type="button" class="btn btn-nuevo">Ahorra S/. 84</button>
                    <br>
                    <button type="button" onclick="contratar('cole_720')" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
                  <div class="col-xs-6">
                    <p>Universidad</p>
                    <p class="precio">S/. 384</p>
                    <button type="button" class="btn btn-nuevo">Ahorra S/. 96</button>
                    <br>
                    <button type="button" onclick="contratar('uni_720')" class="btn btn-contratar" data-toggle="modal" data-target="#myModal">Contratar</button>
                  </div>
              </div>
            </div>
          </div>
          <!--end beneficios-->
        </div>
      </div>
    </div>
  </div>
  <!-- Modal adquiri -->
  <!-- Calendario -->
<script src="js/calendario.js"></script>
<!-- Calendario -->
<script src="js/intranet.js"></script>