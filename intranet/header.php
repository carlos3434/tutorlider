<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" name="viewport" >
  <title>Tutor Líder | <?php echo $titulo ?></title>
  <!-- Latest compiled and minified CSS -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  -->
  <script src="../js/jquery-2.1.3.min.js"></script> 
  <script src="../js/jquery-ui.min.js"></script> 
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/bootstrap.min.js"></script>
  <link rel="shortcut icon" href="../img/favicon.ico">
  <!-- Tutor Líder CSS -->
  <link rel="stylesheet" href="css/estilo.css">
  <!-- Datetime picker -->
  <link rel="stylesheet" href="css/jquery.datetimepicker.css">
  <script src="js/jquery.datetimepicker.js"></script>
  <!-- ionicons Iconos -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->
  <link rel="stylesheet" href='js/datatables/dataTables.bootstrap.css'>

  <script src='js/datatables/jquery.dataTables.min.js'></script> 
  <script src='js/datatables/dataTables.bootstrap.js'></script> 

  <script src="js/menu.js"></script>

  <script type="text/javascript" src="js/admin_ajax.js"></script>
  
  <script type="text/javascript">
  var id ="<?php echo $_SESSION['id']; ?>";
  </script>
</head>
