<!-- /.modal -->
<div class="modal fade" id="planesModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog-lg">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
            <i class="fa fa-close"></i>
        </button>
        <h4 class="modal-title">Alumnos</h4>
      </div>
      <div class="modal-body">
        <form id="form_alumnos" name="form_alumnos" action="" method="post">
          
            <div class="row form-group">

              <div class="col-xs-12">
                  <!-- Inicia contenido -->
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="t_planes" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Tiempo - Nivel</th>
                                    <th>Medio Pago</th>
                                    <th>Estado</th>
                                    <th>Precio registro</th>
                                    <th>Precio voucher</th>
                                    <th> [ Acción ] </th>
                                </tr>
                            </thead>
                            <tbody id="tb_planes">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Tiempo- Nivel</th>
                                    <th>Medio Pago</th>
                                    <th>Estado</th>
                                    <th>Precio registro</th>
                                    <th>Precio voucher</th>
                                    <th> [ Acción ] </th>
                                </tr>
                            </tfoot>
                        </table>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                  <!-- Finaliza contenido -->
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
