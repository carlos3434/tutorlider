<!-- /.modal -->
<div class="modal fade" id="tutoresModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
            <i class="fa fa-close"></i>
        </button>
        <h4 class="modal-title">Tutores</h4>
      </div>
      <div class="modal-body">
        <form id="form_tutor" name="form_tutor" action="" method="post">

          <section class="contenido-panel">
            <div class="contenido">
              <div class="col-md-5 col-sm-12 text-center">
                <img id="foto" src="imagen/avatar.png" class="img-circle" width="100px">
              </div>
              <div class="col-md-7 col-sm-12 text-center">
                <h4>Aun no contamos con tutores para esta clase</h4>
              </div>
            </div>
          </section>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Guardar</button> -->
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
