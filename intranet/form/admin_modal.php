<!-- /.modal -->
<div class="modal fade" id="personaModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
            <i class="fa fa-close"></i>
        </button>
        <h4 class="modal-title">Alumnos</h4>
      </div>
      <div class="modal-body">
        <form id="form_alumnos" name="form_alumnos" action="" method="post">
          
            <div class="row form-group">
              <div class="col-sm-12">
                <div class="col-sm-6">
                  <label class="control-label">Nombre
                      <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombre" id="txt_nombre">
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Apellidos
                      <a id="error_apellido" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Apellido Paterno">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese Apellido Paterno" name="txt_apellido" id="txt_apellido">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="col-sm-6">
                  <label class="control-label">Rol:
                  </label>
                  <select class="form-control" name="slct_rol_id" id="slct_rol_id">
                  </select>
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Email
                      <a id="error_email" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese email">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese email" name="txt_email" id="txt_email">
                </div>
              </div>
              <div class="col-sm-12">

                <div class="col-sm-6">
                  <label class="control-label">Email Confirmado:
                  </label>
                  <select class="form-control" name="slct_email_confirmado" id="slct_email_confirmado">
                      <option value='0' selected>NO</option>
                      <option value='1'>SI</option>
                  </select>
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Estado:
                  </label>
                  <select class="form-control" name="slct_estado" id="slct_estado">
                      <option value='0'>Inactivo</option>
                      <option value='1' selected>Activo</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 tutor">

                <div class="col-sm-6">
                  <label class="control-label">Area:
                  </label>
                  <select class="form-control" multiple='multiple' name="slct_area[]" id="slct_area">
                  </select>
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Nivel:
                  </label>
                  <select class="form-control" multiple='multiple' name="slct_nivel[]" id="slct_nivel">
                  </select>
                </div>
              </div>
              <div class="col-sm-12 tutor">
                <div class="col-sm-12">
                  <label class="control-label">Descripción:
                      <a id="error_descripcion" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese descripcion">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese descripcion" name="txt_descripcion" id="txt_descripcion">
                </div>
              </div>
              <div class="col-sm-12 tutor">
                <div class="col-sm-6">
                  <label class="control-label">Universidad:
                      <a id="error_universidad" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese universidad">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese universidad" name="txt_universidad" id="txt_universidad">
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Carrera:
                      <a id="error_carrera" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese carrera">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese carrera" name="txt_carrera" id="txt_carrera">
                </div>
              </div>
              <div class="col-sm-12 alumno">
                <div class="col-sm-9">
                  <label class="control-label">Link
                      <a id="error_link" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese link">
                          <i class="fa fa-exclamation"></i>
                      </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese link" name="txt_link" id="txt_link">
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Estado link:
                  </label>
                  <select class="form-control" name="slct_estado_link" id="slct_estado_link">
                      <option value='0'>Inactivo</option>
                      <option value='1' selected>Activo</option>
                  </select>
                </div>
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
