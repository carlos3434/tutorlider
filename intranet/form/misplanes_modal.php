<!-- /.modal -->
<div class="modal fade" id="misplanesModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
            <i class="fa fa-close"></i>
        </button>
        <h4 class="modal-title">Voucher</h4>
      </div>
      <div class="modal-body">
        <div class="row form-group"> 
          
          <form id="form_misplanes" name="form_misplanes" action="area/imagen" enctype="multipart/form-data" method="post" >
            <div class="col-sm-12">
                <label class="control-label">Imagen
                </label></br>
                <img id="voucher" src="" class="img-thumbnail" />
            </div>
            <div class="col-sm-12 upload" style="display:none;">
                <input type="file" id="upload" name="upload" accept="image/*" >
                <input type='hidden' name='upload_id' id='upload_id'>
            </div>
          </form>
        </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="subir_voucher();">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
