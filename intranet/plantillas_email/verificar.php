<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta http-equiv="Content-Type" content="text/html;" charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
      <title>Mensaje de Tutorlider.com</title>
      <style>
      @media only screen and (max-width: 300px){
          body {
            width:218px !important;
            margin:auto !important;
          }
          .table {
            width:195px !important;
            margin:auto !important;
          }
          .logo, .titleblock, .linkbelow,
          .box, .footer, .space_footer{
            width:auto !important;
            display: block !important;
          }
          span.title{
            font-size:20px !important;
            line-height: 23px !important
          }
          span.subtitle{
            font-size: 14px !important;
            line-height: 18px !important;
            padding-top:10px !important;
            display:block !important;
          }
          td.box p{
            font-size: 12px !important;
            font-weight: bold !important;
          }
          .table-recap table, .table-recap thead,
          .table-recap tbody, .table-recap th,
          .table-recap td, .table-recap tr {
            display: block !important;
          }
          .table-recap{
            width: 200px!important;
          }
          .table-recap tr td, .conf_body td{
            text-align:center !important;
          }
          .address{
            display: block !important;
            margin-bottom: 10px !important;
          }
          .space_address{
            display: none !important;
          }
        }
      @media only screen and (min-width: 301px) and (max-width: 500px) {
          body {
            width:308px!important;
            margin:auto!important;
          }
          .table {
            width:285px!important;
            margin:auto!important;
          }
          .logo, .titleblock, .linkbelow, 
          .box, .footer, .space_footer{
            width:auto!important;
            display: block!important;
          }
          .table-recap table, .table-recap thead,
          .table-recap tbody, .table-recap th,
          .table-recap td, .table-recap tr {
            display: block !important;
          }
          .table-recap{
            width: 293px !important;
          }
          .table-recap tr td, .conf_body td{
            text-align:center !important;
          }
        }
      @media only screen and (min-width: 501px) and (max-width: 768px) {
          body {
            width:478px!important;
            margin:auto!important;
          }
          .table {
            width:450px!important;
            margin:auto!important;
          }
          .logo, .titleblock, .linkbelow,
          .box, .footer, .space_footer{
            width:auto!important;display: block!important;
          }
        }
      @media only screen and (max-device-width: 480px) {
          body {
            width:308px!important;margin:auto!important;
          }
          .table {
            width:285px;margin:auto!important;
          }
          .logo, .titleblock, .linkbelow,
          .box, .footer, .space_footer{
            width:auto!important;
            display: block!important;
          }
          .table-recap{
            width: 285px!important;
          }
          .table-recap tr td, .conf_body td{
            text-align:center!important;
          }
          .address{
            display: block !important;
            margin-bottom: 10px !important;
          }
          .space_address{
            display: none !important;
          }
        }
  </style>

  </head>
  <body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
    <table class="table table-mail" style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
      <tr>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        <td align="center" style="padding:7px 0">
          <table class="table" bgcolor="#ffffff" style="width:100%">
            <tr>
              <td align="center" class="logo" style="border-bottom:4px solid #00B0F0;padding:7px 0; background-color:black">
                <a title="Tutor Líder" href="<?php echo $tutorlider ?>" style="color:#337ff1">
                  <img src="<?php echo $tutorlider.'/img/logo_email.png'?>" alt="Tutor Líder" width="300px;" />
                </a>
              </td>
            </tr>
            <tr>
              <td align="center" class="titleblock" style="padding:7px 0">
                <font size="2" face="Open-sans, sans-serif" color="#555454">
                  <span class="title" style="font-weight:500;font-size:28px;text-transform:uppercase;line-height:33px">Hola, <?php echo $nombre ?></span><br/>
                  <span class="subtitle" style="font-weight:500;font-size:16px;text-transform:uppercase;line-height:25px">Gracias por la creación de su cuenta en Tutorlider.com</span>
                </font>
              </td>
            </tr>
            <tr>
              <td class="space_footer" style="padding:0!important">&nbsp;</td>
            </tr>
            <tr>
              <td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
                <table class="table" style="width:100%">
                  <tr>
                    <td width="10" style="padding:7px 0">&nbsp;</td>
                    <td style="padding:7px 0">
                      <font size="2" face="Open-sans, sans-serif" color="#555454">
                        <p data-html-only="1" style="border-bottom:1px solid #D6D4D4;margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px">
                          Verificación de correo electrónico </p>
                        <span style="color:#777">
                          Para culminar el proceso y por su seguridad, porfavor verifique su email:<br />
                        </span>
                        <br>
                        <p style="width:100%; text-align:center; margin:0px 0px;"><a style="background-color:#00B0F0; padding:10px 15px; color:#fff; text-decoration:none; margin-top:10px;font-size:16px;text-transform:uppercase;font-weight:600" href=<?php echo $url ?>>Verificar</a>
                        </p>
                      </font>
                    </td>
                    <td width="10" style="padding:7px 0">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="space_footer" style="padding:0!important">&nbsp;</td>
            </tr>
            <tr>
              <td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
                <table class="table" style="width:100%">
                  <tr>
                    <td width="10" style="padding:7px 0">&nbsp;</td>
                    <td style="padding:7px 0">
                      <font size="2" face="Open-sans, sans-serif" color="#555454">
                        <p style="border-bottom:1px solid #D6D4D4;margin:3px 0 7px;text-transform:uppercase;font-weight:600;font-size:16px;padding-bottom:10px">IMPORTANTES CONSEJOS DE SEGURIDAD:</p>
                        <ol style="margin-bottom:0">
                          <li>Mantenga siempre detalles de su cuenta segura.</li>
                          <li>Nunca revele sus datos de acceso a cualquier persona.</li>
                          <li>Cambie su contraseña con regularidad.</li>
                          <li>En caso de que usted sospecha que alguien está usando su cuenta de forma ilegal, por favor notifíquelo inmediatamente.</li>
                        </ol>
                      </font>
                    </td>
                    <td width="10" style="padding:7px 0">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="space_footer" style="padding:0!important">&nbsp;</td>
            </tr>
            <tr>
              <td class="linkbelow" style="padding:7px 0">
                <font size="2" face="Open-sans, sans-serif" color="#555454">
                  <span>Si usted no realizo ningun registro, por favor escribános a alertas@tutorlider.com, y mencione su problema. Posiblemente alguien esta utilizando su correo de forma ilicita.</span>
                </font>
              </td>
            </tr>
            <tr>
              <td class="space_footer" style="padding:0!important">&nbsp;</td>
            </tr>
            <tr>
              <td class="footer" style="border-top:4px solid #00B0F0;padding:7px 0; text-align:justify; font-size:11px">
                <span> La Información contenida en este correo es para uso exclusivo del destinatario y puede ser confidencial. En caso de recibir este correo por error, por favor no imprima, copie, reenvíe o divulgue de manera total o parcial este mensaje. Borre este correo y todas las copias y avise al remitente. Gracias.</span>
                <br>
                <span>The information contained in this e-mail is for the exclusive use of the intended recipient(s) and may be confidential. If you receive this message in error, please do not directly or indirectly use, print, copy, forward, or disclose any part of this message. Please also delete this e-mail and all copies and notify the sender. Thank you.</span>
                <br>
                <span><img src="<?php echo $tutorlider.'/img/vive_verde.png'?>"> No imprima este correo a menos que sea necesario. <a href="<?php echo $tutorlider ?>" style="color:#337ff1">Tutorlider &copy; </a> contribuyendo con el medio ambiente </span>
              </td>
            </tr>
          </table>
        </td>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
<?php
$body = ob_get_clean();