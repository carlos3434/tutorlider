<?php
session_start();
include('conexion.php');
include('llave.php');
if (isset($_SESSION['correo']) and isset($_SESSION['pass'])) {
  $correo = $_SESSION['correo'];
  $pass = $_SESSION['pass'];
} elseif (isset($_POST['usuario']) and isset($_POST['contra'])) {
  $correo = $_POST['usuario'];
  $pass = $_POST['contra'];
} else {
  session_destroy();
  header('location:../login.html');
}
if (isset($_POST['precio_contrato']) and $_POST['precio_contrato'] != "registro") {
  $_SESSION['contrato'] = $_POST['precio_contrato'];
}

$consulta = "SELECT id,nombres,apellido,correo,email_confirmado, estado,rol_id
             FROM usuarios where correo = '$correo'";

if ($resultado = $mysqli->query($consulta)) {
  $numerodefilas = $resultado->num_rows;
    /* obtener el array de objetos */
  if ($numerodefilas == 1) {
    while ($fila = $resultado->fetch_row()) {

      $contra = $fila[4];
      $a = $mysqli->query(
          "SELECT AES_DECRYPT(contrasena,'$llave')
                           FROM usuarios where id=$fila[0]"
      );
      while ($ROW = $a->fetch_row()) {
        $laverdadera = $ROW[0];
      }
      if ($pass == $laverdadera) {
        $_SESSION['login'] = "si";
        $_SESSION['id'] = $fila[0];
        $_SESSION['nombre'] = $fila[1];
        $_SESSION['apellido'] = $fila[2];
        $_SESSION['correo'] = $fila[3];
        $_SESSION['email_confirmado'] = $fila[4];
        $_SESSION['estado'] = $fila[5];
        $_SESSION['rol_id'] = $fila[6];
        $opciones = "SELECT o.*
                      FROM usuario_opcion up JOIN opciones o on up.opcion_id=o.id
                      where usuario_id = $fila[0] AND up.estado =1 AND o.estado=1";
        if ($resultado2 = $mysqli->query($opciones)) :
          while ($filas = $resultado2->fetch_row()) {
              $accesos[] = $filas;
          }
        endif;
        $_SESSION['accesos'] = $accesos;

        header('location:index.php');
      } else {
        session_destroy();
        header('location:../index.html');
      }
    }
  } else {
    //si no existe el usuario
    session_destroy();
    header('location:../index.html');
  }
}
/* liberar el conjunto de resultados */
$resultado->close();
