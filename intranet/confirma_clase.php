<?php 
if (isset($_GET['usuarioId']) and isset($_GET['claseId'])) {
    //el tutor confirma la clase por email
    $usuarioId= $_GET['usuarioId'];
    $claseId = $_GET['claseId'];
    include('conexion.php');
    $sql = "SELECT id FROM clases where id = ? AND id_tutor is null";
    $prepare = $mysqli->prepare($sql);
    $prepare->bind_param('s', $claseId);
    $prepare->execute();
    $prepare->store_result();
    $rows=$prepare->num_rows;
    //$rows = $prepare->affected_rows;
    $prepare->close();
    if ($rows == 1) {
        $sql = "INSERT INTO  clase_tutor (id_tutor, id_clase, estado)
                VALUES (?,?,1) ";
        $updated = $mysqli->prepare($sql);
        $updated->bind_param('ss', $usuarioId, $claseId);
        $rst = $updated->execute();
        if (!$rst) {
            //printf("Errormessage: %s\n", $mysqli->error);
            printf("Errormessage: %s\n", "Se produjo un error");
        } else {
            header('location:index.php');
        }
    }
}