<?php
session_start();
date_default_timezone_set('America/Lima');
include('conexion.php');
include('llave.php');
require 'libs/PHPMailer/PHPMailerAutoload.php';
//$id_alumno = $_SESSION['id'];
$nombre = $_POST['name'];
$apellido = $_POST['apellido'];
$correo = $_POST['email'];
$contrasena = $_POST['pass'];
if (isset($_POST['precio_contrato']) and $_POST['precio_contrato'] != "registro") {
  $_SESSION['contrato'] = $_POST['precio_contrato'];
}

// generador de token
function genera_random($longitud)
{
    $exp_reg="[^A-Z0-9]";
    return substr(
        preg_replace($exp_reg, "", md5(rand())) .
        preg_replace($exp_reg, "", md5(rand())) .
        preg_replace($exp_reg, "", md5(rand())),
        0, $longitud
    );
}
$gen_token = genera_random(40);
//end generador de token
$enviar = "INSERT INTO usuarios (nombres, apellido, correo, contrasena, token, email_confirmado, estado, rol_id)
VALUES ('$nombre', '$apellido', '$correo', AES_ENCRYPT('$contrasena','$llave'), '$gen_token', 0, 1 ,1)";

$resultado = $mysqli->query($enviar);
if ($resultado) :
  # code...
  $id=$_SESSION['id'] = $mysqli->insert_id;
  $url ="http://".$_SERVER['HTTP_HOST']."/intranet/verificar_email.php?id=".$id."&token=".$gen_token;
  $_SESSION['login'] = 'si';
  $_SESSION['correo'] = $correo;
  $_SESSION['pass'] = $contrasena;
  $opciones="INSERT INTO usuario_opcion (usuario_id,opcion_id,estado) 
            VALUES ($id,1,1),($id,2,1),($id,3,1),($id,4,1),($id,6,1)";
  $resultado2 = $mysqli->query($opciones);
  //correo confirmacion
  $mail = new PHPMailer;
  //$mail->SMTPDebug = 3;                               // Enable verbose debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'empresarial.peruvirtual.com';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'alertas@tutorlider.com';                 // SMTP username
  $mail->Password = '20094107c';                           // SMTP password
  $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 465;                                    // TCP port to connect to
  $mail->From = 'alertas@tutorlider.com';
  $mail->FromName = 'Tutor Líder';
  $mail->addAddress($correo, $nombre);     // Add a recipient
  //$mail->addCC('cc@example.com');
  //$mail->addBCC('bcc@example.com');
  //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
  //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
  $mail->isHTML(true);
  $mail->CharSet = 'UTF-8';                                // Set email format to HTML
  $mail->Subject = 'Verificación de Correo ';
  require 'plantillas_email/verificar.php';
  $mail->Body=$body;
  //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
  $mail->send();
  //end envio de correo
endif;

header('location:login.php');