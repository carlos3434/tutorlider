<?php
$tutorlider='http://www.tutorlider.com';
session_start();
include('../conexion.php');
include('../llave.php');
require '../libs/PHPMailer/PHPMailerAutoload.php';
// generador de token
function genera_random($longitud)
{
    $exp_reg="[^A-Z0-9]";
    return substr(
        preg_replace($exp_reg, "", md5(rand())) .
        preg_replace($exp_reg, "", md5(rand())) .
        preg_replace($exp_reg, "", md5(rand())),
        0, $longitud
    );
}
$gen_token = genera_random(40);
if (isset($_POST['accion'])) {

  if ($_POST['accion']=='crear_plan') :
    $id_alumno = $_SESSION['id'];
    $tipoPlan = $_POST['tipo_plan'];
    $medioPagoId = $_POST['medio_pago_id'];
    $nivelId = $_POST['nivel'];
    $precioContrata = $_POST['precio_contrata'];
    //validar si tiene un plan en estado creado
    $sql ="SELECT id FROM plan 
           WHERE alumno_id='$id_alumno' AND estado_pago_id=1 AND estado=1";

    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        echo json_encode(array('rst'=>0,'msj'=>'Ud. ya tiene un plan pendiente de pago'));
        exit();
      }
    }
    
    //estado_pago_id 1 (creado)
    $sql = "INSERT INTO  plan (fecha_registro, tipo_plan_id, medio_pago_id,
            estado_pago_id, nivel_id, precio_registro, estado, alumno_id)
            VALUES (now(),?,?,1,?,?,1,?) ";
    $updated = $mysqli->prepare($sql);
    $updated->bind_param('sssss', $tipoPlan, $medioPagoId,$nivelId,$precioContrata,$id_alumno);
    $rst = $updated->execute();
    if ($rst) {
      echo json_encode(array('rst'=>1));
    }
  endif;
  
  if ($_POST['accion']=='aprobar_voucher') :
    $rst=0;
    $id = $_POST['id'];
    $alumnoId = $_POST['alumno_id'];
    $precioVoucher = $_POST['precio_voucher'];
    $minutos = $_POST['minutos'];
    //validar voucher si plan esta en estado_pago 2 (aprobado)
    $sql = "UPDATE plan set  estado_pago_id=3, precio_voucher=?
            WHERE id =? and estado_pago_id=2";
    $prepare = $mysqli->prepare($sql);
    $prepare->bind_param('ss', $precioVoucher, $id);

    $rst=$prepare->execute();
    if ($rst) {
      $sql="UPDATE usuarios SET minutos=IFNULL(minutos,0)+? where id=?";
      $prepare = $mysqli->prepare($sql);
      $prepare->bind_param('ss', $minutos, $alumnoId);

      $rst=$prepare->execute();
      if ($rst) {
        $rst=1;
      }
    }
    echo json_encode(array('rst'=>$rst));
  endif;
  if ($_POST['accion']=='subir_voucher') :
    //subir_voucher
    $rst=0;
    $id = $_POST['id_plan'];
    $uploadFolder = '../../img/voucher';

    if ( !is_dir($uploadFolder) ) {
        mkdir($uploadFolder);
    }
    if (isset($_FILES['voucher']['name'])) {
      $nameFile = $_FILES['voucher']['name'];
      $extFile = end((explode(".", $nameFile)));
      $tmpFile = $_FILES['voucher']['tmp_name'];
      $fechaFile = new DateTime();
      $fechaFile=$fechaFile->getTimestamp();
      $name=$id.$fechaFile.'.'.$extFile;
      $url=$uploadFolder . '/'.$name;

      if (!move_uploaded_file($tmpFile, $url)){
        $name='';
      } else {
        //se bedera actualizar en la tabla plan
        //estado_pago_id _< 2 con voucer
        $sql = "UPDATE plan set voucher=?,
                 fecha_voucher=now(),
                 estado_pago_id=2
                WHERE id =? ";
        $prepare = $mysqli->prepare($sql);
        $prepare->bind_param('ss', $name, $id);

        $rst=$prepare->execute();
        //$prepare->affected_rows;
        if ($rst) {
          $sql ="SELECT 
                  p.fecha_voucher, p.precio_registro, 
                  CONCAT(u.nombres,u.apellido)
                 FROM plan p 
                 INNER JOIN usuarios u ON p.alumno_id=u.id
                 WHERE p.id ='$id'";

          if ($resultado = $mysqli->query($sql)) { 
            $numerodefilas = $resultado->num_rows;
            if ($numerodefilas > 0) {
              while ($fila = $resultado->fetch_row()) {
                //correo: enmviar correo con los datos de la clase
                //$curso $tema $fecha $hora $tiempo
                $fecha_voucher =$fila[0];
                $precio_voucher =$fila[1];
                $alumno =$fila[2];
                $name = $tutorlider.'/img/voucher/'.$name;
                //enviar correo a pagos@tutorlider.com con el voucher
                $pagos='pagos@tutorlider.com';
                //$pagos='carlos34343434@gmail.com';
                $mail = new PHPMailer;
                $mail->isSMTP();
                $mail->Host = 'empresarial.peruvirtual.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'alertas@tutorlider.com';
                $mail->Password = '20094107c';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;
                $mail->From = 'alertas@tutorlider.com';
                $mail->FromName = 'Tutor Líder';
                $mail->addAddress($pagos, "Pagos");
                $mail->isHTML(true);
                $mail->CharSet = 'UTF-8';
                $mail->Subject = 'Se subio Voucher ';
                require "../plantillas_email/enviar_voucher.php";
                $mail->Body=$body;
                $mail->send();
              }
            }
          }
          $rst=1;
        }
      }
    } else {
      $name='';
    }
    echo json_encode(array('rst'=>$rst));
  endif;
  if ($_POST['accion']=='cargar_usuario') :

    if (isset($_POST['id_usuario'])) {
      $id = $_POST['id_usuario'];
      $sql = "SELECT u.id, email_confirmado, u.estado, rol_id,
              IFNULL(u.nombres,'') AS nombres,
              IFNULL(apellido,'') AS apellido,
              IFNULL(correo,'') AS correo,
              IFNULL(foto,'') AS foto,
              IFNULL(descripcion,'') AS descripcion,
              IFNULL(universidad,'') AS universidad,
              IFNULL(carrera,'') AS carrera,
              IFNULL(r.nombre,'') AS rol,
              (SELECT GROUP_CONCAT( area_id SEPARATOR ',') FROM usuario_area WHERE tutor_id=u.id AND estado=1) AS areas,
              (SELECT GROUP_CONCAT( nivel_id SEPARATOR ',') FROM usuario_nivel WHERE tutor_id=u.id AND estado=1) AS niveles,
              IFNULL(u.minutos,'0') AS minutos,
              IFNULL(u.link,'') AS link,
              IFNULL(u.estado_link,'') AS estado_link
              FROM usuarios u
              LEFT JOIN roles r ON u.rol_id=r.id
              WHERE u.id = '$id'";
    } else {
      $sql = "SELECT  u.id, email_confirmado, u.estado, rol_id,
              IFNULL(u.nombres,'') AS nombres,
              IFNULL(apellido,'') AS apellido,
              IFNULL(correo,'') AS correo,
              IFNULL(foto,'') AS foto,
              IFNULL(descripcion,'') AS descripcion,
              IFNULL(universidad,'') AS universidad,
              IFNULL(carrera,'') AS carrera,
              IFNULL(r.nombre,'') AS rol,
              (SELECT GROUP_CONCAT( area_id SEPARATOR ',') FROM usuario_area WHERE tutor_id=u.id AND estado=1) AS areas,
              (SELECT GROUP_CONCAT( nivel_id SEPARATOR ',') FROM usuario_nivel WHERE tutor_id=u.id AND estado=1) AS niveles,
              IFNULL(u.minutos,'0') AS minutos,
              IFNULL(u.link,'') AS link,
              IFNULL(u.estado_link,'') AS estado_link
              FROM usuarios u
              LEFT JOIN roles r ON u.rol_id=r.id
              ORDER BY id DESC";

    }
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_row()) {
          $result[] = array(
            "id"=>$fila[0],
            "email_confirmado"=>$fila[1],
            "estado"=>$fila[2],
            "rol_id"=>$fila[3],
            "nombres"=>$fila[4],
            "apellido"=>$fila[5],
            "email"=>$fila[6],
            "foto"=>$fila[7],
            "descripcion"=>$fila[8],
            "universidad"=>$fila[9],
            "carrera"=>$fila[10],
            "rol"=>$fila[11],
            "areas"=>$fila[12],
            "niveles"=>$fila[13],
            "minutos"=>$fila[14],
            "link"=>$fila[15],
            "estado_link"=>$fila[16]
            );
        }
        echo json_encode($result);
      }
    }
  endif;
  //consultar estado vigente y si tiene plan aprobado
  if ($_POST['accion']=='consultarEstado_usuario') :
    $id = $_POST['id'];
    $sql = "SELECT u.estado 
            FROM usuarios u 
            where u.id='$id' AND u.minutos>0";
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_row()) {
          $result = array(
            "estado"=>$fila[0]
            );
        }
        echo json_encode($result);
      } else {
        
        echo json_encode(array("estado"=>0));
      }
    }
  endif;
  if ($_POST['accion']=='cambiarEstado_usuario') :
    $estado = $_POST['estado'];
    $id = $_POST['id'];

    $prepare = $mysqli->prepare("UPDATE usuarios set estado=? where id =? ");
    $prepare->bind_param('ss', $estado, $id);

    $rst=$prepare->execute();
    //$prepare->affected_rows;
    if ($rst) {
      echo json_encode($rst);
    }
  endif;
  if ($_POST['accion']=='editar_usuario') :
    $id = $_POST['id'];
    $nombres = $_POST['nombres'];
    $apellido = $_POST['apellido'];
    $correo = $_POST['correo'];
    $estado = $_POST['estado'];
    $emailConfirmado = $_POST['email_confirmado'];
    $descripcion = $_POST['descripcion'];
    $universidad = $_POST['universidad'];
    $rol_id = $_POST['rol_id'];
    $carrera = $_POST['carrera'];
    $estadoLink = $_POST['estado_link'];
    $link = $_POST['link'];

    $uploadFolder = '../../img/fotos';

    if ( !is_dir($uploadFolder) ) {
        mkdir($uploadFolder);
    }

    if (isset($_FILES['foto']['name'])) {
      $nameFile = $_FILES['foto']['name'];
      $extFile = end((explode(".", $nameFile)));
      $tmpFile = $_FILES['foto']['tmp_name'];
      $fechaFile = new DateTime();
      $fechaFile=$fechaFile->getTimestamp();
      $name=$id.$fechaFile.'.'.$extFile;
      $url=$uploadFolder . '/'.$name;

      if (!move_uploaded_file($tmpFile, $url)){
        $name='';
      }
    } else {
      $name='';
    }
    if (isset($_POST['contrasena']) && $_POST['contrasena']!='' ) {
      $contrasena = $_POST['contrasena'];
      $sql = "UPDATE usuarios
              SET nombres=? , apellido=?, correo=?, estado=?, rol_id=?, foto=?,
                  email_confirmado=?,carrera=?, descripcion=?, universidad=?,
                  contrasena =AES_ENCRYPT(?,?), estado_link=?, link=?
              WHERE id =? ";
      $prepare = $mysqli->prepare($sql);
      $prepare->bind_param(
          'sssssssssssssss', $nombres, $apellido, $correo, $estado, $rol_id, $name,
          $emailConfirmado, $carrera, $descripcion, $universidad,
          $contrasena, $llave, $estadoLink, $link, $id
      );
    } else {
      $sql = "UPDATE usuarios
              SET nombres=? , apellido=?, correo=?, estado=?, rol_id=?, foto=?,
                  email_confirmado=?,carrera=?, descripcion=?, universidad=?,
                  estado_link=?, link=?
              WHERE id =? ";
      $prepare = $mysqli->prepare($sql);
      $prepare->bind_param(
          'sssssssssssss', $nombres, $apellido, $correo, $estado, $rol_id, $name,
          $emailConfirmado, $carrera, $descripcion, $universidad,
          $estadoLink, $link, $id
      );

    }

    $rst=$prepare->execute();

    if ($rst) {
      //insertar registro en niveles y area
      $sql ="UPDATE usuario_area SET estado=0 WHERE tutor_id=?";
      $prepare = $mysqli->prepare($sql);
      $prepare->bind_param('s', $id);
      $updated=$prepare->execute();
      if ($updated) {
        if (isset($_POST['area'])) {
          $area = $_POST['area'];

          $areas = explode(',', $area);

          foreach ($areas as $key => $value) {
            $sql = "SELECT id FROM usuario_area WHERE area_id='$value' AND tutor_id='$id'";
            if ($resultado = $mysqli->query($sql)) {
              $numerodefilas = $resultado->num_rows;
              if ($numerodefilas > 0) {
                $sql ="UPDATE usuario_area SET estado=1 WHERE area_id='$value' AND tutor_id='$id'";
                $mysqli->query($sql);
              } else {
                $sql ="INSERT INTO usuario_area (area_id,tutor_id,estado) VALUES ('$value','$id',1) ";
                $mysqli->query($sql);
              }
            }
          }
        }
      }
      $sql ="UPDATE usuario_nivel SET estado=0 WHERE tutor_id=?";
      $prepare = $mysqli->prepare($sql);
      $prepare->bind_param('s', $id);
      $updated=$prepare->execute();
      if ($updated) {
        if (isset($_POST['nivel'])) {
          $nivel = $_POST['nivel'];

          $niveles = explode(',', $nivel);

          foreach ($niveles as $key => $value) {
            $sql = "SELECT id FROM usuario_nivel WHERE nivel_id='$value' AND tutor_id='$id'";
            if ($resultado = $mysqli->query($sql)) {
              $numerodefilas = $resultado->num_rows;
              if ($numerodefilas > 0) {
                $sql ="UPDATE usuario_nivel SET estado=1 WHERE nivel_id='$value' AND tutor_id='$id'";
                $mysqli->query($sql);
              } else {
                $sql ="INSERT INTO usuario_nivel (nivel_id,tutor_id,estado)  VALUES ('$value','$id',1) ";
                $mysqli->query($sql);
              }
            }
          }
        }
      }

      echo json_encode($rst);
    }
  endif;

  /////////////////inicio agendar/////////////////
  if ($_POST['accion']=='agendar_clase') :

    $id_alumno = $_SESSION['id'];
    $curso = $_POST['curso'];
    $tema = $_POST['tema'];
    $nivel = $_POST['nivel'];

    $tiempo = $_POST['tiempo'];
    $fecha = $_POST['fecha'];
    $hora = $_POST['hora'];
    $area = $_POST['area'];

    $horas = $_POST['horas'];
    $minutos = $_POST['minutos'];
    //validar si tiene suficientes minutos

    $sql="SELECT u.estado
          from usuarios u
          where  u.estado=1 AND u.id='$id_alumno'
          and u.minutos >=$tiempo";
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {

        $uploadFolder = '../../img/archivos';

        if ( !is_dir($uploadFolder) ) {
            mkdir($uploadFolder);
        }

        if (isset($_FILES['archivo']['name'])) {
          $nameFile = $_FILES['archivo']['name'];
          $extFile = end((explode(".", $nameFile)));
          $tmpFile = $_FILES['archivo']['tmp_name'];
          $fechaFile = new DateTime();
          $fechaFile=$fechaFile->getTimestamp();
          $name=$id_alumno.$fechaFile.'.'.$extFile;
          $url=$uploadFolder . '/'.$name;

          if (!move_uploaded_file($tmpFile, $url))
              $name='';
        } else {
          $name='';
        }
        $enviar = "INSERT INTO clases (id_alumno, curso, tema, archivo_ruta,
                   tiempo, dia, hora, id_nivel, id_area, horas, minutos)
                  VALUES ('$id_alumno', '$curso', '$tema', '$name', '$tiempo',
                   '$fecha', '$hora', '$nivel', '$area', '$horas', '$minutos')";
        $resultado = $mysqli->query($enviar);
        $claseId = $mysqli->insert_id;

        $enviar = "INSERT INTO clase_estado (clase_id, estado_id, estado)
                    VALUES ($claseId,1, 1)";//agendado

        $resultado = $mysqli->query($enviar);

        //disminuir el numero de minutos del plan aprobado mas reciente
        $sql="UPDATE usuarios SET minutos=minutos-? where id=?";
        $prepare = $mysqli->prepare($sql);
        $prepare->bind_param('ss', $tiempo, $id_alumno);

        $rst=$prepare->execute();

        // buscar los tutores que tengan asignado el nivel y area correspondiente
        $sql ="SELECT u.id, u.correo, u.nombres
               FROM usuarios u
               INNER JOIN usuario_area ua ON u.id=ua.tutor_id AND ua.area_id='$area'
               INNER JOIN usuario_nivel un ON u.id=un.tutor_id AND un.nivel_id='$nivel'
               WHERE rol_id='3' AND u.estado =1 AND ua.estado=1 AND un.estado=1";
        if ($resultado = $mysqli->query($sql)) {
          $numerodefilas = $resultado->num_rows;
          if ($numerodefilas > 0) {

            while ($fila = $resultado->fetch_row()) {
              //correo: enviar correo con los datos de la clase
              //$curso $tema $fecha $hora $tiempo
              $usuarioId =$fila[0];
              $correo =$fila[1];
              $nombre =$fila[2];

              $url =$tutorlider."/intranet/confirma_clase.php?usuarioId=".$usuarioId."&claseId=".$claseId;
              $archivo=$tutorlider."/img/archivos/".$name;
              
              $mail = new PHPMailer;
              $mail->isSMTP();
              $mail->Host = 'empresarial.peruvirtual.com';
              $mail->SMTPAuth = true;
              $mail->Username = 'alertas@tutorlider.com';
              $mail->Password = '20094107c';
              $mail->SMTPSecure = 'ssl';
              $mail->Port = 465;
              $mail->From = 'alertas@tutorlider.com';
              $mail->FromName = 'Tutor Líder';
              $mail->addAddress($correo, $nombre);
              $mail->AddAttachment($archivo,$archivo);
              //$mail->AddAttachment($_FILES["archivo"]["tmp_name"],$_FILES["archivo"]["name"]);
              $mail->isHTML(true);
              $mail->CharSet = 'UTF-8';
              $mail->Subject = 'Apuntate ';
              require "../plantillas_email/consultar_tutor_agenda.php";
              $mail->Body=$body;
              $mail->send();
            }
          }
        }
        if ($resultado) {
          echo json_encode("1");
        } else {
          echo json_encode("0");
        }
      } else {
          //no tiene minutos
          echo json_encode("2");
      }
      
    } else {
      echo json_encode("0");
    }


  endif;

  if ($_POST['accion']=='cargar_rol') :
    $sql ="SELECT id, nombre FROM roles WHERE estado = 1";
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_row()) {
          $result[] = array(
            "id"=>$fila[0],
            "nombre"=>$fila[1]
            );
        }
        echo json_encode($result);
      }
    }
  endif;
  /////////////////////////fin////////////////////////

  if ($_POST['accion']=='nuevo_usuario') :
    $rol_id = $_POST['rol_id'];
    $nombres = $_POST['nombres'];
    $apellido = $_POST['apellido'];
    $correo = $_POST['correo'];
    $estado = $_POST['estado'];
    $emailConfirmado = $_POST['email_confirmado'];
    $descripcion = $_POST['descripcion'];
    $universidad = $_POST['universidad'];
    $carrera = $_POST['carrera'];
    $nivel = $_POST['nivel'];
    $area = $_POST['area'];
    $estadoLink = $_POST['estado_link'];
    $link = $_POST['link'];


    $sql = "INSERT INTO usuarios (rol_id,nombres,apellido,correo,estado,
            email_confirmado,descripcion,universidad,carrera, contrasena, token
            , estado_link, link)
            VALUES ('$rol_id','$nombres','$apellido','$correo','$estado',
            '$emailConfirmado','$descripcion','$universidad','$carrera',
            AES_ENCRYPT('123456','$llave'), '$gen_token','$estadoLink','$link') ";
    $rst = $mysqli->query($sql);
    //tienes que insertar al menu que tendra accesos
    $id= $mysqli->insert_id;
    if ($rol_id==1) {//alumno
      $opciones="INSERT INTO usuario_opcion (usuario_id,opcion_id,estado)
                 VALUES ($id,1,1),($id,2,1),($id,3,1),($id,4,1),($id,6,1)";
      $resultado2 = $mysqli->query($opciones);
    } elseif ($rol_id==2) {//admin
      $opciones="INSERT INTO usuario_opcion (usuario_id,opcion_id,estado)
                  VALUES ($id,5,1),($id,6,1)";
      $resultado2 = $mysqli->query($opciones);

    } elseif ($rol_id==3) {//tutor
      $opciones="INSERT INTO usuario_opcion (usuario_id,opcion_id,estado)
                  VALUES ($id,6,1)";
      $resultado2 = $mysqli->query($opciones);
      //insertar areas y niveles
      $nivel = (isset($nivel)) ? $nivel : '' ;
      $area = (isset($area)) ? $area : '' ;
      if (isset($nivel)) {
        $nivel = explode(',', $nivel);

        foreach ($nivel as $key => $value) {
          $sql ="INSERT INTO usuario_nivel (nivel_id,tutor_id,estado) VALUES ('$value','$id',1) ";
          $mysqli->query($sql);
        }
      }
      if (isset($area)) {
        $areas = explode(',', $area);
        //var_dump($areas);
        foreach ($areas as $key => $value) {
          $sql ="INSERT INTO usuario_area (area_id,tutor_id,estado) VALUES ('$value','$id',1) ";
          $mysqli->query($sql);
        }
      }
    }
    if ($rst) {
        echo json_encode($rst);
    } else {
        echo json_encode($sql);

    }
  endif;
  if ($_POST['accion']=='cargar_planes') :
    $id_alumno = $_POST['id_alumno'];
    $sql = "SELECT p.id, p.fecha_registro, tp.minutos, n.nombre as nivel,
            ep.nombre as estado_pago, ep.alerta, p.precio_registro, p.alumno_id,
            p.precio_voucher, mp.nombre AS medio_pago, p.voucher, p.estado_pago_id
            FROM plan p
            JOIN tipo_plan tp ON p.tipo_plan_id=tp.id
            JOIN medio_pago mp ON p.medio_pago_id=mp.id
            JOIN niveles n ON p.nivel_id=n.id
            JOIN estado_pago ep ON p.estado_pago_id=ep.id
            WHERE p.alumno_id = '$id_alumno' AND p.estado=1";
    $result = arraY();
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_object()) {
          $result[] = array(
            "id"=>$fila->id,
            "alumno_id"=>$fila->alumno_id,
            "fecha_registro"=>$fila->fecha_registro,
            "minutos"=>$fila->minutos,
            "nivel"=>$fila->nivel,
            "estado_pago_id"=>$fila->estado_pago_id,
            "estado_pago"=>$fila->estado_pago,
            "alerta"=>$fila->alerta,
            "precio_registro"=>$fila->precio_registro,
            "precio_voucher"=>$fila->precio_voucher,
            "medio_pago"=>$fila->medio_pago,
            "voucher"=>$fila->voucher
          );
        }
      }
    }
    echo json_encode(array('rst'=>1,'data'=>$result));
  endif;

  //////////////////////cargar clases para elegir tutor////////////////////
  if ($_POST['accion']=='cargar_clases_con_detalle') :
    $id_alumno = $_POST['id_alumno'];
    $estado_id = $_POST['estado_id'];
    $sql = "SELECT c.id, IFNULL(c.curso,''), IFNULL(c.tema,''), IFNULL(c.dia,''),
            IFNULL(c.hora,''), IFNULL(c.tiempo,''), IFNULL(c.id_tutor,''),
            IFNULL(u.nombres,''), IFNULL(u.apellido,''), IFNULL(u.descripcion,''),
            IFNULL(u.universidad,''), IFNULL(u.carrera,''),
            IFNULL(u.foto,''),
             (SELECT IFNULL(GROUP_CONCAT(
                CONCAT(
                  IFNULL(us.id,''),'-',
                  IFNULL(us.nombres,''),'-',
                  IFNULL(us.apellido,''),'-',
                  IFNULL(us.carrera,''),'-',
                  IFNULL(us.universidad,''),'-',
                  IFNULL(us.foto,'')
                )
              SEPARATOR '|'),'')
              FROM usuarios us
              JOIN clase_tutor ct ON us.id=ct.id_tutor
              WHERE ct.id_clase=c.id AND us.estado=1 AND ct.estado=1
            ) AS tutores
            FROM clases c
            LEFT JOIN
              ( SELECT MAX(id) AS id,clase_id
                FROM clase_estado
                GROUP BY clase_id
              ) ce2 ON c.id=ce2.clase_id
            LEFT JOIN clase_estado ce ON ce2.id=ce.id
            LEFT JOIN usuarios u ON c.id_tutor=u.id
            WHERE c.id_alumno='$id_alumno' AND ce.estado=1
            AND ce.estado_id='$estado_id'
            ORDER BY c.id desc
            LIMIT 5";
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_row()) {
          $result[] = array(
            "id"=>$fila[0],
            "curso"=>$fila[1],
            "tema"=>$fila[2],
            "dia"=>$fila[3],
            "hora"=>$fila[4],
            "tiempo"=>$fila[5],
            "id_tutor"=>$fila[6],
            "nombres"=>$fila[7],
            "apellido"=>$fila[8],
            "descripcion"=>$fila[9],
            "universidad"=>$fila[10],
            "carrera"=>$fila[11],
            "foto"=>$fila[12],
            "tutores"=>$fila[13]
            );
        }
        echo json_encode(array('rst'=>1,'data'=>$result));
        //echo json_encode($result);
      } else {
        echo json_encode(array('rst'=>0,'data'=>''));
      }
    } else {
        echo json_encode("error en consulta");
    }
  endif;
  //////////////////////cargar clases////////////////////
  if ($_POST['accion']=='cargar_clases') :
    $id_alumno = $_POST['id_alumno'];
    $estado_id = $_POST['estado_id'];
    if ($estado_id==2) {//confirmadas
      $sql = "SELECT c.id, IFNULL(c.curso,''), IFNULL(c.tema,''), IFNULL(c.dia,''),
            IFNULL(c.hora,''), IFNULL(c.tiempo,''), IFNULL(c.id_tutor,''),
            IFNULL(u.nombres,''), IFNULL(u.apellido,''), IFNULL(u.descripcion,''),
            IFNULL(u.universidad,''), IFNULL(u.carrera,''),
            IFNULL(u.foto,''), IFNULL(c.archivo_ruta,''), 
            IF(ua.estado_link=1,ua.link,'') link
            FROM clases c
            LEFT JOIN
              ( SELECT MAX(id) AS id,clase_id
                FROM clase_estado
                GROUP BY clase_id
              ) ce2 ON c.id=ce2.clase_id
            LEFT JOIN clase_estado ce ON ce2.id=ce.id
            LEFT JOIN usuarios u ON c.id_tutor=u.id
            LEFT JOIN usuarios ua ON c.id_alumno=ua.id
            WHERE c.id_alumno='$id_alumno' AND ce.estado=1
            AND STR_TO_DATE(c.dia,'%d-%m-%Y') + INTERVAL c.`horas` HOUR+ INTERVAL c.`minutos` MINUTE + INTERVAL c.`tiempo` MINUTE >=NOW()
            AND ce.estado_id='$estado_id'";
    } else {

      $sql = "SELECT c.id, IFNULL(c.curso,''), IFNULL(c.tema,''), IFNULL(c.dia,''),
            IFNULL(c.hora,''), IFNULL(c.tiempo,''), IFNULL(c.id_tutor,''),
            IFNULL(u.nombres,''), IFNULL(u.apellido,''), IFNULL(u.descripcion,''),
            IFNULL(u.universidad,''), IFNULL(u.carrera,''),
            IFNULL(u.foto,''), IFNULL(c.archivo_ruta,''), 
            IF(ua.estado_link=1,ua.link,'') link
            FROM clases c
            LEFT JOIN
              ( SELECT MAX(id) AS id,clase_id
                FROM clase_estado
                GROUP BY clase_id
              ) ce2 ON c.id=ce2.clase_id
            LEFT JOIN clase_estado ce ON ce2.id=ce.id
            LEFT JOIN usuarios u ON c.id_tutor=u.id
            LEFT JOIN usuarios ua ON c.id_alumno=ua.id
            WHERE c.id_alumno='$id_alumno' AND ce.estado=1
            AND ce.estado_id='$estado_id'";
    }
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_row()) {
          $result[] = array(
            "id"=>$fila[0],
            "curso"=>$fila[1],
            "tema"=>$fila[2],
            "dia"=>$fila[3],
            "hora"=>$fila[4],
            "tiempo"=>$fila[5],
            "id_tutor"=>$fila[6],
            "nombres"=>$fila[7],
            "apellido"=>$fila[8],
            "descripcion"=>$fila[9],
            "universidad"=>$fila[10],
            "carrera"=>$fila[11],
            "foto"=>$fila[12],
            "archivo_ruta"=>'../img/archivos/'.$fila[13],
            //"ruta_aula"=>$fila[14],
            "link"=>$fila[14]
            );
        }
        echo json_encode(array('rst'=>1,'data'=>$result));
        //echo json_encode($result);
      } else {
        echo json_encode(array('rst'=>0,'data'=>''));
      }
    } else {
        echo json_encode("error en consulta");
    }
  endif;
  //////////////////////elegir tutor////////////////////
  if ($_POST['accion']=='elegir_tutor') :
    $idClase = $_POST['id_clase'];
    $idTutor = $_POST['id_tutor'];
    $sql = "UPDATE clases
            set id_tutor = '$idTutor'
            where id ='$idClase' ";
    $rst = $mysqli->query($sql);
    if ($rst) {
      //insertar estado de clase a confirmada
      $enviar = "INSERT INTO clase_estado (clase_id, estado_id, estado)
                 VALUES ($idClase,2, 1)";//agendado
      $rst2 = $mysqli->query($enviar);

        //echo json_encode($rst2); exit();
      if ($rst2) {
        //enviar correo de confirmacion al docente
        $sql = "SELECT c.curso,c.tema, c.archivo_ruta,
                c.tiempo, c.dia, c.hora, u.nombres, u.correo
                FROM clases c JOIN  usuarios u ON c.id_tutor=u.id
                WHERE u.id='$idTutor' AND c.id='$idClase' ";
        if ($resultado = $mysqli->query($sql)) {
          $numerodefilas = $resultado->num_rows;
          if ($numerodefilas > 0) {
            while ($fila = $resultado->fetch_object()) {
              $curso=$fila->curso;
              $tema=$fila->tema;
              $archivoRuta=$fila->archivo_ruta;
              $archivoRuta ="http://".$_SERVER['HTTP_HOST']."/img/archivos/".$archivoRuta;
              $tiempo=$fila->tiempo;
              $fecha=$fila->dia;
              $hora=$fila->hora;
              $nombre=$fila->nombres;
              $correo=$fila->correo;
            }
          }
        }
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = 'empresarial.peruvirtual.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'alertas@tutorlider.com';
        $mail->Password = '20094107c';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->From = 'alertas@tutorlider.com';
        $mail->FromName = 'Tutor Líder';
        $mail->addAddress($correo, $nombre);
        //$mail->addAddress("carlos3434@hotmail.com", $nombre);
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Clase Confirmada ';
        require "../plantillas_email/confirmar_clase_tutor.php";
        $mail->Body=$body;
        $mail->send();
        $result=array('rst'=>1);
        echo json_encode($result);
      }
    }
  endif;
  //////////////////////buscar tutor////////////////////
  if ($_POST['accion']=='buscar_tutor') :
    $id_clase = $_POST['id_clase'];
    //$estado_id = $_POST['estado_id'];
    $sql = "SELECT u.id, IFNULL(u.nombres,''), IFNULL(u.apellido,''),
            IFNULL(u.descripcion,''), IFNULL(u.universidad,''),
            IFNULL(u.carrera,''), IFNULL(u.foto,'')
            FROM usuarios u JOIN clase_tutor ct ON u.id=ct.id_tutor
            WHERE ct.id_clase='$id_clase' AND u.estado=1 AND ct.estado=1";
    if ($resultado = $mysqli->query($sql)) {
      $numerodefilas = $resultado->num_rows;
      if ($numerodefilas > 0) {
        while ($fila = $resultado->fetch_row()) {
          $result[] = array(
            "id"=>$fila[0],
            "nombres"=>$fila[1],
            "apellido"=>$fila[2],
            "descripcion"=>$fila[3],
            "universidad"=>$fila[4],
            "carrera"=>$fila[5],
            "foto"=>$fila[6]
            );
        }
        echo json_encode($result);
      } else {
        echo json_encode("no hay registros");
      }
    } else {
        echo json_encode("error en consulta");
    }
  endif;

} else {
  if (count($_FILES) > 0) {
    # code...
    $uploadFolder = '../../img/fotos';

    if ( !is_dir($uploadFolder) ) {
        mkdir($uploadFolder);
    }
    $id_usuario = $_SESSION['id'];
    $nameFile = $_FILES['Filedata']['name'];
    $extFile = end((explode(".", $nameFile)));
    $tmpFile = $_FILES['Filedata']['tmp_name'];
    $fecha = new DateTime();
    $fecha=$fecha->getTimestamp();
    $name=$id_usuario.$fecha.'.'.$extFile;
    $url=$uploadFolder . '/'.$name;

    if (!move_uploaded_file($tmpFile, $url)) {
        $response =
            array(
                'status'     => false
            );
    } else {
        //imagedestroy($tmpFile);
        $url='/'.$url;
        $result = array(
                    'status'          => true,
                    'src'             => $url
                );
        try {
          //actualizar
          $sql = "UPDATE usuarios
                  SET foto='$url'
                  WHERE id ='$id_usuario' ";
          $rst = $mysqli->query($sql);
        } catch (Exception $e) {
          $result =
            array(
                'status'     => false
            );
          echo json_encode($result);
        }
        echo json_encode($result);
    }
  } else {
    $result=array('rst'=>0,'data'=>"no se encontro accion");

    echo json_encode($result);
  }

}