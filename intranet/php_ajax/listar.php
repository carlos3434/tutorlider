<?php

session_start();
include('../conexion.php');
include('../llave.php');
if (isset($_POST['elemento'])) {
    if ($_POST['elemento']=='nivel') :

        $sql = "SELECT id,nombre,estado FROM niveles";
        if ($resultado = $mysqli->query($sql)) {
          $numerodefilas = $resultado->num_rows;
          if ($numerodefilas > 0) {
            while ($fila = $resultado->fetch_row()) {
              $result[] = array(
                "id"=>$fila[0],
                "nombre"=>$fila[1],
                "estado"=>$fila[2]
                );
            }
            
            echo json_encode(array('rst'=>1,'datos'=>$result));
          }
        }
    endif;
    if ($_POST['elemento']=='area') :
        $sql = "SELECT id,nombre,estado FROM areas";
        if ($resultado = $mysqli->query($sql)) {
          $numerodefilas = $resultado->num_rows;
          if ($numerodefilas > 0) {
            while ($fila = $resultado->fetch_row()) {
              $result[] = array(
                "id"=>$fila[0],
                "nombre"=>$fila[1],
                "estado"=>$fila[2]
                );
            }
            echo json_encode(array('rst'=>1,'datos'=>$result));
          }
        }
    endif;
}