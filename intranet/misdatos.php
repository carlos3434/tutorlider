 <?php
  $id_usuario = $_SESSION['id'];
  ?>
<div class="blockcontenido">
  <section class="contenido">
    <span class="encabezado"><a href="index.php">Home /</a> Mis datos</span>
  </section>
  <section class="contenido">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="box">
                <form id="form_alumnos" name="form_alumnos" action="" method="post" autocomplete="off">
                  <input type="text" style="display: none" id="fakeUsername" name="fakeUsername" value="" />
                  <input type="password" style="display: none" id="fakePassword" name="fakePassword" value="" />
                    <div class="row form-group">
                      <div class="col-sm-12">
                        <div class="col-sm-3">
                          <label class="control-label">Nombres
                              <a id="error_nombres" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombres" id="txt_nombres">
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">Apellidos
                              <a id="error_apellido" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Apellidos">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese Apellidos" name="txt_apellido" id="txt_apellido" autocomplete="off">
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">Conraseña
                              <a id="error_contrasena" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Contraseña">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="password" class="form-control" placeholder="Ingrese Contraseña" name="txt_contrasena" id="txt_contrasena" value="" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <div class="row form-group tutor">
                      <div class="col-sm-12">
                        <div class="col-sm-3">
                          <label class="control-label">Foto
                              <a id="error_foto" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Suba su foto">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <img id="img_foto" src="" class="img-thumbnail" style="width:'600px'; height:'200px';" />
                          <input type="file" id="foto" name="foto" accept="image/*" >
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">Descripción
                              <a id="error_descripcion" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese su descripción">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese su descripción" name="txt_descripcion" id="txt_descripcion">
                        </div>
                      </div>
                    </div>
                    <div class="row form-group tutor">
                      <div class="col-sm-12">
                        <div class="col-sm-3">
                          <label class="control-label">Universidad
                              <a id="error_universidad" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese universidad">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese universidad" name="txt_universidad" id="txt_universidad">
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">Carrera
                              <a id="error_carrera" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese carrera">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese carrera" name="txt_carrera" id="txt_carrera">
                        </div>
                      </div>
                    </div>
                    <div class="row form-group" style="display:none">
                      <div class="col-sm-12">
                        <div class="col-sm-3">
                          <label class="control-label">correo
                              <a id="error_correo" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese correo">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="hidden" class="form-control" placeholder="Ingrese correo" name="txt_correo" id="txt_correo">
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">estado
                              <a id="error_estado" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese estado">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="hidden" class="form-control" placeholder="Ingrese estado" name="txt_estado" id="txt_estado">
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">email_confirmado
                              <a id="error_email_confirmado" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese email_confirmado">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="hidden" class="form-control" placeholder="Ingrese email_confirmado" name="txt_email_confirmado" id="txt_email_confirmado">
                        </div>
                        <div class="col-sm-3">
                          <label class="control-label">rol_id
                              <a id="error_rol_id" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese rol_id">
                                  <i class="fa fa-exclamation"></i>
                              </a>
                          </label>
                          <input type="hidden" class="form-control" placeholder="Ingrese rol_id" name="txt_rol_id" id="txt_rol_id">
                        </div>
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-sm-12">
                          <div class="col-sm-3">
                              <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                          </div>
                      </div>
                    </div>
                    </form>
                </form>
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>
  </section><!-- /.content -->

  <div class="row">
    <div class="col-lg-12">
      <section class="contenido">
        <p>Copyright 2015 © <strong>Tutor Líder</strong></p>
      </section>
    </div>
  </div>
</div>

<script type="text/javascript">
  var id_usuario = "<?php echo $id_usuario; ?>";
  //alert(id_alumno);
</script>
<script type="text/javascript" src="js/misdatos_ajax.js"></script>
<script type="text/javascript" src="js/misdatos.js"></script>