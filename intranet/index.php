<?php
session_start();
if (isset($_GET['sesion']) and $_GET['sesion'] == "no") {
  session_destroy();
  header('location:../index.html');
}
if ( $_SESSION['login'] != "si") {
  session_destroy();
  header('location:../index.html');
}
include('conexion.php');
$id = $_SESSION['id'];
  //confirmadas
  $sql = "SELECT COUNT(ce.id) AS cantidad, ce.estado_id as id
        FROM clases c
        LEFT JOIN
          ( SELECT MAX(id) AS id,clase_id
            FROM clase_estado
            GROUP BY clase_id
          ) ce2 ON c.id=ce2.clase_id
        LEFT JOIN clase_estado ce ON ce2.id=ce.id
        LEFT JOIN usuarios u ON c.id_tutor=u.id
        LEFT JOIN usuarios ua ON c.id_alumno=ua.id
        WHERE c.id_alumno='$id' AND ce.estado=1
        AND STR_TO_DATE(c.dia,'%d-%m-%Y') + INTERVAL c.`horas` HOUR+ INTERVAL c.`minutos` MINUTE + INTERVAL c.`tiempo` MINUTE >=NOW()
        AND ce.estado_id='2'";

  $confirmadas = 0;
  if ($resultado = $mysqli->query($sql)) :
    while ($fila = $resultado->fetch_object()) :
      switch ($fila->id) {
        case '2'://confirmado
            $confirmadas=$fila->cantidad;
            break;
        }
    endwhile;
  endif;

    $estados = "SELECT COUNT(ce.id) AS cantidad, ce.estado_id as id
            FROM clases c
            LEFT JOIN
                (SELECT MAX(id) AS id,clase_id 
                FROM clase_estado 
                GROUP BY clase_id) ce2
             ON c.id=ce2.clase_id
            LEFT JOIN clase_estado ce ON ce2.id=ce.id
            LEFT JOIN usuarios u ON c.id_tutor=u.id
            WHERE c.id_alumno='$id' AND ce.estado=1
            GROUP BY ce.estado_id";
    
$agendadas = 0;
$realizadas = 0;
if ($resultado = $mysqli->query($estados)) :
  while ($fila = $resultado->fetch_object()) :
    switch ($fila->id) {
      case '1'://agenda
          $agendadas=$fila->cantidad;
          break;
      case '3'://realizado
          $realizadas=$fila->cantidad;
          break;
      }
  endwhile;
endif;
$panel="";

//menu izquierdo
//activar o no, titulo
if (isset($_GET['title'])) {
  switch ($_GET['title']) {
     case 'inicio':
       $titulo = 'Panel de control';
       $panel="activo";
         break;
     case 'adminusuarios':
       $titulo = 'administrar usuarios';
       $adminusuarios="activo";
         break;
     case 'agendadas':
       $titulo = 'Clases agendadas';
       $agendadas="activo";
         break;
     case 'confirmadas':
        $titulo = 'Clases confirmadas';
        $confirmadas="activo";
         break;
     case 'elegir':
        $titulo = 'Elegir Tutor';
        $confirmadas="activo";
         break;
     case 'misplanes':
       $titulo = 'Mis planes';
       $misplanes="activo";
         break;
     case 'misdatos':
       $titulo = 'Mi cuenta';
       $misdatos="activo";
         break;
     default:
       $titulo ='Panel de control';
       $panel="activo";
         break;
  }
  //enviar el nombre del titulo
  $titulo=$_GET['title'];
} else {
  $titulo = 'Panel de Control';
  $panel="activo";
}
include('header.php');
include('menu.php');
?>
<body id="body">
<?php
if (isset($_GET['title'])) {
  switch ($_GET['title']) {
     case 'inicio':
       include('inicio.php');
         break;
     //administrar usuarios
     case 'adminusuarios':
       include('adminusuarios.php');
         break;
     case 'agendadas':
       include('agendadas.php');
         break;
     case 'confirmadas':
       include('confirmadas.php');
         break;
     case 'elegir':
       include('elegir.php');
         break;
     case 'misplanes':
       include('misplanes.php');
         break;
     //editar mis datos
     case 'misdatos':
       include('misdatos.php');
         break;
     default:
       include('inicio.php');
         break;
  }
} else {
  include('inicio.php');
}
?>
</body>
</html>