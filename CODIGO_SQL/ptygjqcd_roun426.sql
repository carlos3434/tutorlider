-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-09-2015 a las 23:58:44
-- Versión del servidor: 5.6.23
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ptygjqcd_roun426`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_cache`
--

CREATE TABLE IF NOT EXISTS `rc_cache` (
  `user_id` int(10) unsigned NOT NULL,
  `cache_key` varchar(128) CHARACTER SET ascii NOT NULL,
  `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `expires` datetime DEFAULT NULL,
  `data` longtext NOT NULL,
  KEY `rc_expires_index` (`expires`),
  KEY `rc_user_cache_index` (`user_id`,`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_cache_index`
--

CREATE TABLE IF NOT EXISTS `rc_cache_index` (
  `user_id` int(10) unsigned NOT NULL,
  `mailbox` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `expires` datetime DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  `data` longtext NOT NULL,
  PRIMARY KEY (`user_id`,`mailbox`),
  KEY `rc_expires_index` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_cache_messages`
--

CREATE TABLE IF NOT EXISTS `rc_cache_messages` (
  `user_id` int(10) unsigned NOT NULL,
  `mailbox` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `uid` int(11) unsigned NOT NULL DEFAULT '0',
  `expires` datetime DEFAULT NULL,
  `data` longtext NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`mailbox`,`uid`),
  KEY `rc_expires_index` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_cache_shared`
--

CREATE TABLE IF NOT EXISTS `rc_cache_shared` (
  `cache_key` varchar(255) CHARACTER SET ascii NOT NULL,
  `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `expires` datetime DEFAULT NULL,
  `data` longtext NOT NULL,
  KEY `rc_expires_index` (`expires`),
  KEY `rc_cache_key_index` (`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_cache_thread`
--

CREATE TABLE IF NOT EXISTS `rc_cache_thread` (
  `user_id` int(10) unsigned NOT NULL,
  `mailbox` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `expires` datetime DEFAULT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`user_id`,`mailbox`),
  KEY `rc_expires_index` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_contactgroupmembers`
--

CREATE TABLE IF NOT EXISTS `rc_contactgroupmembers` (
  `contactgroup_id` int(10) unsigned NOT NULL,
  `contact_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`contactgroup_id`,`contact_id`),
  KEY `rc_contactgroupmembers_contact_index` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_contactgroups`
--

CREATE TABLE IF NOT EXISTS `rc_contactgroups` (
  `contactgroup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`contactgroup_id`),
  KEY `rc_contactgroups_user_index` (`user_id`,`del`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_contacts`
--

CREATE TABLE IF NOT EXISTS `rc_contacts` (
  `contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `email` text NOT NULL,
  `firstname` varchar(128) NOT NULL DEFAULT '',
  `surname` varchar(128) NOT NULL DEFAULT '',
  `vcard` longtext,
  `words` text,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `rc_user_contacts_index` (`user_id`,`del`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_dictionary`
--

CREATE TABLE IF NOT EXISTS `rc_dictionary` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `language` varchar(5) NOT NULL,
  `data` longtext NOT NULL,
  UNIQUE KEY `uniqueness` (`user_id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_identities`
--

CREATE TABLE IF NOT EXISTS `rc_identities` (
  `identity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  `standard` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `organization` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL,
  `reply-to` varchar(128) NOT NULL DEFAULT '',
  `bcc` varchar(128) NOT NULL DEFAULT '',
  `signature` longtext,
  `html_signature` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`identity_id`),
  KEY `rc_user_identities_index` (`user_id`,`del`),
  KEY `rc_email_identities_index` (`email`,`del`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `rc_identities`
--

INSERT INTO `rc_identities` (`identity_id`, `user_id`, `changed`, `del`, `standard`, `name`, `organization`, `email`, `reply-to`, `bcc`, `signature`, `html_signature`) VALUES
(1, 1, '2015-06-23 02:50:49', 0, 1, 'Jhony Díaz', 'Tutor Líder', 'jdiaz@tutorlider.com', '', '', '<p></p>\r\n<div style="text-align: center; margin: 0px; font-family: Open-sans, sans-serif">\r\n<div class="contenedor_tutorlider" style="overflow: hidden; width: 100%; background-color: black; font-family: Open-sans, sans-serif; margin: 0px">\r\n<div style="overflow: hidden">\r\n<div class="nombre_tutorlider" style="float: left; width: 60%; padding-top: 10px; padding-bottom: 15px; text-align: center; color: aliceblue">\r\n<p style="margin-bottom: 10px; margin: 0px; font-size: 20px">Jhony Díaz</p>\r\n<p style="margin: 0px; font-size: 14px"><strong>Correo:</strong> jdiaz@tutorlider.com</p>\r\n<p style="margin: 0px; font-size: 14px"><strong>Web Site: </strong><a style="text-decoration: none; color: antiquewhite" href="http://tutorlider.com">http://tutorlider.com</a></p>\r\n</div>\r\n<div class="logo_tutorlider" style="width: 40%; float: left"><a href="http://tutorlider.com"><img style="max-width: 250px; width: 90%; padding-left: 10px" src="http://tutorlider.com/img/logo_email.png" alt="" /></a></div>\r\n</div>\r\n<div class="sociales" style="width: 100%; color: white">\r\n<ul style="list-style: none; margin: 0px 0px"><li style="display: inline; margin-right: 5px"><a href="https://www.facebook.com/pages/Tutor-L%C3%ADder/696611720446948?fref=ts"><img src="http://tutorlider.com/img/facebook.png" alt="" width="30px" height="30px" /></a></li>\r\n<li style="display: inline; margin-right: 5px"><a href="https://pe.linkedin.com/company/tutor-l%C3%ADder"><img src="http://tutorlider.com/img/linkedin.png" alt="" width="30px" height="30px" /></a></li>\r\n<li style="display: inline; margin-right: 5px"><a href="https://plus.google.com/communities/106369053861583255782"><img src="http://tutorlider.com/img/google-plus.png" alt="" width="30px" height="30px" /></a></li>\r\n</ul></div>\r\n</div>\r\n<span class="ambiente_tutorlider" style="font-size: 12px"><img src="http://tutorlider.com/img/vive_verde.png" alt="" /> No imprima este correo a menos que sea necesario. <a href="http://tutorlider.com">Tutorlider ©</a> contribuyendo con el medio ambiente</span></div>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_searches`
--

CREATE TABLE IF NOT EXISTS `rc_searches` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type` int(3) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `data` text,
  PRIMARY KEY (`search_id`),
  UNIQUE KEY `uniqueness` (`user_id`,`type`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_session`
--

CREATE TABLE IF NOT EXISTS `rc_session` (
  `sess_id` varchar(128) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `ip` varchar(40) NOT NULL,
  `vars` mediumtext NOT NULL,
  PRIMARY KEY (`sess_id`),
  KEY `rc_changed_index` (`changed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rc_session`
--

INSERT INTO `rc_session` (`sess_id`, `created`, `changed`, `ip`, `vars`) VALUES
('3c15c7d712cc6d8e3dc848b7221e1b4f', '2015-06-23 04:41:13', '2015-06-23 04:41:13', '181.176.34.170', 'bGFuZ3VhZ2V8czo1OiJlc19FUyI7dGVtcHxiOjE7c2tpbnxzOjU6ImxhcnJ5Ijs=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_system`
--

CREATE TABLE IF NOT EXISTS `rc_system` (
  `name` varchar(64) NOT NULL,
  `value` mediumtext,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rc_system`
--

INSERT INTO `rc_system` (`name`, `value`) VALUES
('roundcube-version', '2015030800');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rc_users`
--

CREATE TABLE IF NOT EXISTS `rc_users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mail_host` varchar(128) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `last_login` datetime DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `preferences` longtext,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`,`mail_host`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `rc_users`
--

INSERT INTO `rc_users` (`user_id`, `username`, `mail_host`, `created`, `last_login`, `language`, `preferences`) VALUES
(1, 'jdiaz@tutorlider.com', 'empresarial.peruvirtual.com', '2015-06-23 01:40:13', '2015-06-23 01:40:13', 'es_ES', 'a:12:{s:10:"htmleditor";i:1;s:17:"reply_same_folder";b:1;s:22:"spellcheck_before_send";b:1;s:10:"reply_mode";i:1;s:12:"preview_pane";b:1;s:17:"message_threading";a:1:{s:5:"INBOX";b:0;}s:11:"client_hash";s:32:"4006c56101d20f70fd0da0ad36104fc6";s:11:"drafts_mbox";s:12:"INBOX.Drafts";s:9:"junk_mbox";s:10:"INBOX.Junk";s:9:"sent_mbox";s:10:"INBOX.Sent";s:10:"trash_mbox";s:11:"INBOX.Trash";s:15:"namespace_fixed";b:1;}');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `rc_cache`
--
ALTER TABLE `rc_cache`
  ADD CONSTRAINT `rc_user_id_fk_cache` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_cache_index`
--
ALTER TABLE `rc_cache_index`
  ADD CONSTRAINT `rc_user_id_fk_cache_index` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_cache_messages`
--
ALTER TABLE `rc_cache_messages`
  ADD CONSTRAINT `rc_user_id_fk_cache_messages` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_cache_thread`
--
ALTER TABLE `rc_cache_thread`
  ADD CONSTRAINT `rc_user_id_fk_cache_thread` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_contactgroupmembers`
--
ALTER TABLE `rc_contactgroupmembers`
  ADD CONSTRAINT `rc_contact_id_fk_contacts` FOREIGN KEY (`contact_id`) REFERENCES `rc_contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rc_contactgroup_id_fk_contactgroups` FOREIGN KEY (`contactgroup_id`) REFERENCES `rc_contactgroups` (`contactgroup_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_contactgroups`
--
ALTER TABLE `rc_contactgroups`
  ADD CONSTRAINT `rc_user_id_fk_contactgroups` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_contacts`
--
ALTER TABLE `rc_contacts`
  ADD CONSTRAINT `rc_user_id_fk_contacts` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_dictionary`
--
ALTER TABLE `rc_dictionary`
  ADD CONSTRAINT `rc_user_id_fk_dictionary` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_identities`
--
ALTER TABLE `rc_identities`
  ADD CONSTRAINT `rc_user_id_fk_identities` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rc_searches`
--
ALTER TABLE `rc_searches`
  ADD CONSTRAINT `rc_user_id_fk_searches` FOREIGN KEY (`user_id`) REFERENCES `rc_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
