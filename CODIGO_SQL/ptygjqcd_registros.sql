-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-09-2015 a las 23:58:32
-- Versión del servidor: 5.6.23
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ptygjqcd_registros`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `registros`
--

INSERT INTO `registros` (`id`, `nombre`, `correo`) VALUES
(1, 'hola', 'jhony@gmail.com'),
(2, 'BernabÃ© Vilcahuaman Medina 200700009k', 'bernabe.vilcahuaman@outlook.com'),
(3, 'a', 'a@amalca.com'),
(4, 'juan palacios', 'juanpalacios34@outlook.com'),
(5, 'carlos juan', 'c@tutorlider.com'),
(6, 'carlos', 'c@tutorlider.com'),
(7, 'carlos', 'carlos_vt1@hotmail.com'),
(8, 'hh', 'carlos_vt1@hotmail.com'),
(9, 'rr', 'carlos_vt1@hotmail.com'),
(10, 'juan', 'j@hotmail.com'),
(11, 'juanes', 'juanes@htv.com'),
(12, 'Adrian', 'asesoriasuni@hotmail.com'),
(13, 'miguel', 'm@uni.pe'),
(14, 'martin', 'martin_trujillo1105@hotmail.com'),
(15, 'Geralf', 'asesor_ger_man@hotmail.com'),
(16, 'VÃ­ctor ', 'ariesold@gmail.com'),
(17, 'jjjjj', 'jj@hotmail.com'),
(18, 'k', 'd@m.m'),
(19, 'eduardo', 'edu@hotmail.com'),
(20, 'Jairzinho', 'jairzinhosantosjsf@gmail.com'),
(21, 'Erick', 'erick7126@gmail.com'),
(22, 'david', 'dragonci0@hotmail.com'),
(23, 'Alejandro Morales', 'cerna_se@hotmail.com'),
(24, 'haydee grimalda  zapata mamani', 'hgzama@hotmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
