-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-09-2015 a las 23:58:23
-- Versión del servidor: 5.6.23
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ptygjqcd_owm234`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatban`
--

CREATE TABLE IF NOT EXISTS `owm_chatban` (
  `banid` int(11) NOT NULL AUTO_INCREMENT,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmtill` datetime DEFAULT '0000-00-00 00:00:00',
  `address` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `blockedCount` int(11) DEFAULT '0',
  PRIMARY KEY (`banid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatconfig`
--

CREATE TABLE IF NOT EXISTS `owm_chatconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vckey` varchar(255) DEFAULT NULL,
  `vcvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `owm_chatconfig`
--

INSERT INTO `owm_chatconfig` (`id`, `vckey`, `vcvalue`) VALUES
(1, 'dbversion', '1.6.10'),
(2, 'featuresversion', '1.6.6'),
(3, 'title', 'Tutor Líder'),
(4, 'hosturl', 'http://tutorlider.com'),
(5, 'logo', 'http://tutorlider.com/img/logo_email.png'),
(6, 'usernamepattern', '{name}'),
(7, 'chatstyle', 'silver'),
(8, 'chattitle', 'Soporte en Línea'),
(9, 'geolink', 'http://api.hostip.info/get_html.php?ip={ip}'),
(10, 'geolinkparams', 'width=440,height=100,toolbar=0,scrollbars=0,location=0,status=1,menubar=0,resizable=1'),
(11, 'max_connections_from_one_host', '10'),
(12, 'thread_lifetime', '600'),
(13, 'email', ''),
(14, 'sendmessagekey', 'enter'),
(15, 'enableban', '0'),
(16, 'enablessl', '0'),
(17, 'forcessl', '0'),
(18, 'usercanchangename', '1'),
(19, 'enablegroups', '0'),
(20, 'enablestatistics', '1'),
(21, 'enablejabber', '0'),
(22, 'enablepresurvey', '0'),
(23, 'surveyaskmail', '0'),
(24, 'surveyaskgroup', '1'),
(25, 'surveyaskmessage', '0'),
(26, 'surveyaskcaptcha', '0'),
(27, 'enablepopupnotification', '0'),
(28, 'showonlineoperators', '0'),
(29, 'enablecaptcha', '1'),
(30, 'online_timeout', '30'),
(31, 'updatefrequency_operator', '2'),
(32, 'updatefrequency_chat', '2'),
(33, 'updatefrequency_oldchat', '7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatgroup`
--

CREATE TABLE IF NOT EXISTS `owm_chatgroup` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `vcemail` varchar(64) DEFAULT NULL,
  `vclocalname` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vclocaldescription` varchar(1024) NOT NULL,
  `vccommondescription` varchar(1024) NOT NULL,
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatgroupoperator`
--

CREATE TABLE IF NOT EXISTS `owm_chatgroupoperator` (
  `groupid` int(11) NOT NULL,
  `operatorid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatmessage`
--

CREATE TABLE IF NOT EXISTS `owm_chatmessage` (
  `messageid` int(11) NOT NULL AUTO_INCREMENT,
  `threadid` int(11) NOT NULL,
  `ikind` int(11) NOT NULL,
  `agentId` int(11) NOT NULL DEFAULT '0',
  `tmessage` text NOT NULL,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `tname` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`messageid`),
  KEY `idx_agentid` (`agentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Volcado de datos para la tabla `owm_chatmessage`
--

INSERT INTO `owm_chatmessage` (`messageid`, `threadid`, `ikind`, `agentId`, `tmessage`, `dtmcreated`, `tname`) VALUES
(1, 4, 3, 0, 'Vistante fue redireccionado de la pagina http://tutorlider.com/index.html\nhttp://tutorlider.com/intranet/index.php', '2015-06-23 13:03:37', NULL),
(2, 4, 4, 0, 'Gracias por ponerse en contacto con nosotros. El operador estara con usted en breve...', '2015-06-23 13:03:37', NULL),
(3, 4, 3, 0, 'El visitante se traslado a http://tutorlider.com/index.html', '2015-06-23 13:04:02', NULL),
(4, 4, 3, 0, 'El visitante se traslado a http://tutorlider.com/index.html', '2015-06-23 13:04:11', NULL),
(5, 4, 3, 0, 'El visitante se traslado a http://tutorlider.com/index.html', '2015-06-23 13:07:21', NULL),
(6, 4, 3, 0, 'Visitante cerrando ventana de conversación', '2015-06-23 13:04:15', NULL),
(7, 5, 3, 0, 'Vistante fue redireccionado de la pagina http://tutorlider.com/index.html\nhttp://tutorlider.com/intranet/index.php', '2015-06-23 13:29:22', NULL),
(8, 5, 4, 0, 'Gracias por ponerse en contacto con nosotros. El operador estara con usted en breve...', '2015-06-23 13:29:22', NULL),
(9, 5, 1, 0, 'Hola Jhony', '2015-06-23 13:29:32', 'Visitante'),
(10, 5, 6, 0, 'Operador Administrator Entrando a la conversación', '2015-06-23 13:29:37', NULL),
(11, 5, 2, 1, 'Hola Jose', '2015-06-23 13:29:56', 'Administrator'),
(12, 5, 1, 0, 'Soy José Luis', '2015-06-23 13:29:59', 'Visitante'),
(13, 5, 1, 0, 'que genial esta herramienta', '2015-06-23 13:30:25', 'Visitante'),
(14, 5, 2, 1, 'si es excelente', '2015-06-23 13:30:46', 'Administrator'),
(15, 5, 6, 0, 'Visitante cambio su nombre Visitante por José Luis', '2015-06-23 13:31:04', NULL),
(16, 5, 6, 0, 'Visitante conectado a la conversación nuevamente', '2015-06-23 13:31:15', NULL),
(17, 5, 1, 0, 'Sobre el chat tengo algunos comentario', '2015-06-23 13:31:49', 'José Luis'),
(18, 5, 1, 0, 's', '2015-06-23 13:31:52', 'José Luis'),
(19, 5, 2, 1, 'si dime', '2015-06-23 13:32:18', 'Administrator'),
(20, 5, 6, 0, 'Visitante conectado a la conversación nuevamente', '2015-06-23 13:32:28', NULL),
(21, 5, 3, 0, 'El visitante se traslado a http://tutorlider.com/', '2015-06-23 13:32:40', NULL),
(22, 5, 6, 0, 'Visitante conectado a la conversación nuevamente', '2015-06-23 13:33:02', NULL),
(23, 5, 1, 0, 'solo con fines de ser más empáticos con el usuario', '2015-06-23 13:33:58', 'José Luis'),
(24, 5, 1, 0, 'en lugar de contacte a un operador lo más adecuado sería HABLA CON NOSOTROS', '2015-06-23 13:34:33', 'José Luis'),
(25, 5, 2, 1, 'no entendí bien', '2015-06-23 13:36:06', 'Administrator'),
(26, 5, 1, 0, 'en la imagen del chat dice:', '2015-06-23 13:36:35', 'José Luis'),
(27, 5, 1, 0, 'Ayuda en vivo:  EN LINEA CONTACTE A UN OPERADOR', '2015-06-23 13:36:56', 'José Luis'),
(28, 5, 1, 0, 'En la última parte creo que lo más adecuado sería poner HABLA CON NOSOTROS', '2015-06-23 13:37:18', 'José Luis'),
(29, 5, 2, 1, 'la imagen claro', '2015-06-23 13:37:48', 'Administrator'),
(30, 5, 2, 1, 'se puede cambiar', '2015-06-23 13:37:58', 'Administrator'),
(31, 5, 1, 0, 'y hay otras opciones para la cara de la chica ', '2015-06-23 13:38:28', 'José Luis'),
(32, 5, 1, 0, 'Esto y los  bordes de la pestaña si fuera posible hacerlos con chaflan(bordeado) no en punta', '2015-06-23 13:40:03', 'José Luis'),
(33, 5, 2, 1, 'en realidad es la imagen inicial con la que viene esta herramienta', '2015-06-23 13:40:04', 'Administrator'),
(34, 5, 2, 1, 'falta cambiar algunas cosas', '2015-06-23 13:40:24', 'Administrator'),
(35, 5, 1, 0, 'esto por neuromarketing', '2015-06-23 13:40:33', 'José Luis'),
(36, 5, 1, 0, 'ya entonces eso será un detalle que se puede ajustar después por ahora solo el texto que te comente de habla con nosotros.', '2015-06-23 13:41:07', 'José Luis'),
(37, 5, 2, 1, 'si los bordes redondeados', '2015-06-23 13:41:09', 'Administrator'),
(38, 5, 1, 0, 'para terminar lo del chat', '2015-06-23 13:41:50', 'José Luis'),
(39, 5, 1, 0, 'cuando no hay nadie dice fuera de línea creo que sería más adecuado lo siguiente:', '2015-06-23 13:43:18', 'José Luis'),
(40, 5, 1, 0, 'Ayuda en vivo(queda igual) después Fuera de línea y cambiamos el último por escríbenos aquí ', '2015-06-23 13:46:25', 'José Luis'),
(41, 5, 5, 0, 'El Operador tiene problemas de conexión, esta usted temporalmente en espera. Lo sentimos por la demora.', '2015-06-23 13:46:42', NULL),
(42, 5, 6, 0, 'Operador Administrator de regreso', '2015-06-23 13:46:57', NULL),
(43, 5, 2, 1, 'Listo yo lo modifico4', '2015-06-23 13:47:47', 'Administrator'),
(44, 5, 1, 0, 'vamos al core de nuestro de negocio.', '2015-06-23 13:47:57', 'José Luis'),
(45, 5, 1, 0, 'algo breve', '2015-06-23 13:48:00', 'José Luis'),
(46, 5, 5, 0, 'El Operador tiene problemas de conexión, esta usted temporalmente en espera. Lo sentimos por la demora.', '2015-06-23 13:48:19', NULL),
(47, 5, 1, 0, '¡que falta para que este completo el proceso de reserva de una clase y poder probarlo', '2015-06-23 13:48:22', 'José Luis'),
(48, 5, 6, 0, 'Operador Administrator de regreso', '2015-06-23 13:50:09', NULL),
(49, 5, 2, 1, 'la lista de tutores nada más', '2015-06-23 13:50:30', 'Administrator'),
(50, 5, 1, 0, 'cómo tienes pensado implementar esta parte?', '2015-06-23 13:50:58', 'José Luis'),
(51, 5, 5, 0, 'El Operador tiene problemas de conexión, esta usted temporalmente en espera. Lo sentimos por la demora.', '2015-06-23 13:51:34', NULL),
(52, 5, 6, 0, 'Operador Administrator de regreso', '2015-06-23 13:51:41', NULL),
(53, 5, 2, 1, 'una lista de tutores con una materia', '2015-06-23 13:51:48', 'Administrator'),
(54, 5, 2, 1, 'y enviarles mensajes', '2015-06-23 13:51:56', 'Administrator'),
(55, 5, 2, 1, 'al correo para que acepten', '2015-06-23 13:52:02', 'Administrator'),
(56, 5, 1, 0, 'ya entonces lo haremos manual hasta crear su intranet donde ellos verán las clases disponibles y aplicar para que el usuario eliga', '2015-06-23 13:53:12', 'José Luis'),
(57, 5, 2, 1, 'claro', '2015-06-23 13:53:38', 'Administrator'),
(58, 5, 1, 0, 'genial pero mientras tanto hay que crearles sus perfiles  para que el usuario pueda escoger.', '2015-06-23 13:54:33', 'José Luis'),
(59, 5, 1, 0, 'y a medida de confirman al correo hacerles aparecer', '2015-06-23 13:54:56', 'José Luis'),
(60, 5, 2, 1, 'si necesito la lista de tutores con su perfil ', '2015-06-23 13:55:24', 'Administrator'),
(61, 5, 2, 1, 'y bueno su correo', '2015-06-23 13:55:33', 'Administrator'),
(62, 5, 1, 0, 'todavía no hemos confirmado a ningún tutor pero hay unos 5 para hacerlo, con el fin de no tenerlos en para.', '2015-06-23 13:56:38', 'José Luis'),
(63, 5, 2, 1, 'aun me falta corregir algunos detalles de la plataforma pero son mínimos', '2015-06-23 13:58:18', 'Administrator'),
(64, 5, 5, 0, 'El Operador tiene problemas de conexión, esta usted temporalmente en espera. Lo sentimos por la demora.', '2015-06-23 13:58:55', NULL),
(65, 5, 1, 0, 'hay que hacer una prueba completa desde registrarse hasta agendar clase aunque sea con dos perfiles ficticios. ', '2015-06-23 13:59:52', 'José Luis'),
(66, 5, 1, 0, 'lo podemos hacer ahora o dime la hora Jhony?', '2015-06-23 14:00:06', 'José Luis'),
(67, 5, 3, 0, 'El visitante se traslado a http://tutorlider.com/', '2015-06-23 14:05:47', NULL),
(68, 5, 3, 0, 'El visitante se traslado a http://tutorlider.com/index.html', '2015-06-23 14:07:00', NULL),
(69, 5, 3, 0, 'El visitante se traslado a http://tutorlider.com/index.html', '2015-06-23 14:14:02', NULL),
(70, 5, 6, 0, 'Visitante conectado a la conversación nuevamente', '2015-06-23 14:14:15', NULL),
(71, 5, 6, 0, 'Visitante conectado a la conversación nuevamente', '2015-06-23 14:14:28', NULL),
(72, 5, 6, 0, 'Visitante José Luis salió de la conversación', '2015-06-23 14:14:44', NULL),
(73, 6, 3, 0, 'Vistante fue redireccionado de la pagina http://tutorlider.com/\nhttps://www.facebook.com/', '2015-08-26 23:17:09', NULL),
(74, 6, 3, 0, 'Correo Electrónico: centenovsky@lammer.org', '2015-08-26 23:17:09', NULL),
(75, 6, 1, 0, 'ola amigo, nezesito un maiztro de lenguaje pz', '2015-08-26 23:17:09', 'jhon centeno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatnotification`
--

CREATE TABLE IF NOT EXISTS `owm_chatnotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) DEFAULT NULL,
  `vckind` varchar(16) DEFAULT NULL,
  `vcto` varchar(256) DEFAULT NULL,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `vcsubject` varchar(256) DEFAULT NULL,
  `tmessage` text NOT NULL,
  `refoperator` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatoperator`
--

CREATE TABLE IF NOT EXISTS `owm_chatoperator` (
  `operatorid` int(11) NOT NULL AUTO_INCREMENT,
  `vclogin` varchar(64) NOT NULL,
  `vcpassword` varchar(64) NOT NULL,
  `vclocalename` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vcemail` varchar(64) DEFAULT NULL,
  `dtmlastvisited` datetime DEFAULT '0000-00-00 00:00:00',
  `istatus` int(11) DEFAULT '0',
  `vcavatar` varchar(255) DEFAULT NULL,
  `vcjabbername` varchar(255) DEFAULT NULL,
  `iperm` int(11) DEFAULT '65535',
  `inotify` int(11) DEFAULT '0',
  `dtmrestore` datetime DEFAULT '0000-00-00 00:00:00',
  `vcrestoretoken` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`operatorid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `owm_chatoperator`
--

INSERT INTO `owm_chatoperator` (`operatorid`, `vclogin`, `vcpassword`, `vclocalename`, `vccommonname`, `vcemail`, `dtmlastvisited`, `istatus`, `vcavatar`, `vcjabbername`, `iperm`, `inotify`, `dtmrestore`, `vcrestoretoken`) VALUES
(1, 'tutorlider', '404f214d50138c5828849f2c401f7639', 'Administrator', 'Administrator', 'contacto@tutorlider.com', '2015-06-23 13:53:48', 0, '', '', 65535, 0, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatresponses`
--

CREATE TABLE IF NOT EXISTS `owm_chatresponses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `vcvalue` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `owm_chatresponses`
--

INSERT INTO `owm_chatresponses` (`id`, `locale`, `groupid`, `vcvalue`) VALUES
(1, 'es', NULL, 'Hola, en que puedo ayudarle?'),
(2, 'es', NULL, 'Hola! Bienvenido a nuestra ayuda en vivo. En que puedo ayudarle ?');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatrevision`
--

CREATE TABLE IF NOT EXISTS `owm_chatrevision` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `owm_chatrevision`
--

INSERT INTO `owm_chatrevision` (`id`) VALUES
(672);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owm_chatthread`
--

CREATE TABLE IF NOT EXISTS `owm_chatthread` (
  `threadid` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(64) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `agentName` varchar(64) DEFAULT NULL,
  `agentId` int(11) NOT NULL DEFAULT '0',
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmmodified` datetime DEFAULT '0000-00-00 00:00:00',
  `lrevision` int(11) NOT NULL DEFAULT '0',
  `istate` int(11) NOT NULL DEFAULT '0',
  `ltoken` int(11) NOT NULL,
  `remote` varchar(255) DEFAULT NULL,
  `referer` text,
  `nextagent` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(8) DEFAULT NULL,
  `lastpinguser` datetime DEFAULT '0000-00-00 00:00:00',
  `lastpingagent` datetime DEFAULT '0000-00-00 00:00:00',
  `userTyping` int(11) DEFAULT '0',
  `agentTyping` int(11) DEFAULT '0',
  `shownmessageid` int(11) NOT NULL DEFAULT '0',
  `userAgent` varchar(255) DEFAULT NULL,
  `messageCount` varchar(16) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  PRIMARY KEY (`threadid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `owm_chatthread`
--

INSERT INTO `owm_chatthread` (`threadid`, `userName`, `userid`, `agentName`, `agentId`, `dtmcreated`, `dtmmodified`, `lrevision`, `istate`, `ltoken`, `remote`, `referer`, `nextagent`, `locale`, `lastpinguser`, `lastpingagent`, `userTyping`, `agentTyping`, `shownmessageid`, `userAgent`, `messageCount`, `groupid`) VALUES
(1, 'Jhony', '55867ff7c92ee6.84710647', NULL, 0, '2015-06-21 05:14:37', '2015-06-21 05:14:37', 6, 5, 10163821, '190.43.66.193', 'http://tutorlider.com/chat/operator/getcode.php?lang=es&i=mgreen&style=silver\nhttp://tutorlider.com/chat/operator/getcode.php?lang=es&i=simple&style=silver', 0, 'es', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', NULL, NULL),
(2, 'Jesus', '55867ff7c92ee6.84710647', 'Administrator', 1, '2015-06-21 05:27:52', '2015-06-21 05:30:53', 57, 3, 1222184, '190.43.66.193', 'http://tutorlider.com/', 0, 'es', '2015-06-21 05:31:09', '2015-06-21 05:30:53', 0, 0, 6, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', '5', NULL),
(3, 'Jesus', '55867ff7c92ee6.84710647', 'Administrator', 1, '2015-06-21 05:40:14', '2015-06-21 05:42:05', 92, 3, 5446599, '190.43.66.193', 'http://tutorlider.com/index.html\nhttp://tutorlider.com/conocenos.html', 0, 'es', '0000-00-00 00:00:00', '2015-06-21 05:42:07', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', '0', NULL),
(4, 'Jesus', '55867ff7c92ee6.84710647', NULL, 0, '2015-06-23 13:03:37', '2015-06-23 13:03:40', 131, 0, 11859937, '181.176.35.204', 'http://tutorlider.com/index.html\nhttp://tutorlider.com/intranet/index.php', 0, 'es', '0000-00-00 00:00:00', '2015-06-23 13:24:41', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36', NULL, NULL),
(5, 'José Luis', '5589957aaf0c93.42106383', 'Administrator', 1, '2015-06-23 13:29:22', '2015-06-23 14:14:44', 671, 3, 5641420, '190.222.139.7', 'http://tutorlider.com/index.html\nhttp://tutorlider.com/intranet/index.php', 0, 'es', '2015-06-23 14:14:45', '0000-00-00 00:00:00', 0, 0, 9, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36', '27', NULL),
(6, 'jhon centeno', '55de80ef267a51.36089365', NULL, 0, '2015-08-26 23:17:09', '2015-08-26 23:17:09', 672, 5, 10606410, '181.65.8.232', 'http://tutorlider.com/\nhttps://www.facebook.com/', 0, 'es', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
