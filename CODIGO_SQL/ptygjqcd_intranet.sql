-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generaci贸n: 28-09-2015 a las 23:58:14
-- Versi贸n del servidor: 5.6.23
-- Versi贸n de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ptygjqcd_intranet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(300) COLLATE utf8_bin NOT NULL,
  `apellido` varchar(300) COLLATE utf8_bin NOT NULL,
  `correo` varchar(200) COLLATE utf8_bin NOT NULL,
  `contrasena` varbinary(2000) NOT NULL,
  `token` varchar(40) COLLATE utf8_bin NOT NULL,
  `email_confirmado` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=52 ;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `nombres`, `apellido`, `correo`, `contrasena`, `token`, `email_confirmado`) VALUES
(9, 'jose', 'luis', 'joseluism4_uni@hotmail.com', '鉼滤𚇰忖夏#鱂', '', 0),
(10, 'juan', 'perez', 'juan@hotmail.com', '鉼滤𚇰忖夏#鱂', '', 0),
(11, 'Carlos', 'Vilcahuaman Tovar', 'carlos_vt1@hotmail.com', '91eb86a309e06ecf58e3965967ff9b0a', '', 0),
(12, 'Jhoel', 'Rubi帽os', 'j.rubinos@hotmail.com', '�3宱厪鼩�5%�8淀6', '', 0),
(13, 'Tutor', 'L铆der', 'aprendiendoyresolviendocontigo@gmail.com', '�#3LW︸D46]�溞M7椔坹欩t雊e�.>si芋k閻EQE��"琊�}q陛�臕X�	1QK3悚麎铖\0MY;+3^�r櫕!殛FQew遬�籜~�^妄\n螿Ufdz~o3胺-�(口u�湝冕\rgR}\0臨Oe欪謊q�崲>�;迬酵_|慱f绱骪r�蛒�3''�&扖D鵐t�!<E餋�	D伫W4謜>紞>�', '', 0),
(14, 'v', 'v', 'v@h', '0~r埨俓r箔鳏訣*m', '', 0),
(16, 'Jose Luis', 'Vilcahuaman Tovar', 'arcangel_ejso@hotmail.com', 'd蠥鈫蟂ミ朜�1埛�烈婰.L酯菻�3驤豛ZOg嗼su}妊�G潘�:V菺駪髼潙g鉳\0胺6�8V渕�3灐檃C揪遨q叡�穕�"m帋矣偗己�螚嗍鏺�1橤頺瞫遶Q��21僅伙	h鍊羂nT譻堷�*穘斱~庵&�5潇證�僼R譢r氭衳Jf1�0洈_f�E	媇�|J頬|鯿', '', 0),
(17, 'g', 'g', 'g@tutorlider.com', '緉eIjBk�2l*憾{�', '', 0),
(18, 'gddd', 'g', 'g@tutorlider.comd', '\nzZ酷kh�z齔{�)�', '', 0),
(19, 'StartupOne', 'Uni', 'startuponeuni@gmail.com', 'L里匴o砛\\\�o0R押�,�&濦L�5s�)漢漬�(偑�想褰<魯墰XN鲀\ny<呃~u輗5倨y磒瓣%徾Y硥�蹫]�斀ッ鵅�%覶u畬,Z�踌EG鄤焻}鉶釥�<麍汨努�A\rr铊鶈}浭\Z拓"xY摕欞$s绪黣M招\n虖���)Q镉Μ[Hd鐄鯤芻v�<眉秂5Y�`��搁维湝;L;h', '', 0),
(25, 'Jhony Urbano', 'Diaz Quispe', 'jhony_2505@hotmail.com', '綵5?濰鑨�$囶5', 'ec2c5a2384098b970bf7356e4ee5587c35ecc9be', 1),
(26, 'Jhony Urbano', 'Diaz quispe', 'jhonyfiqt@gmail.com', '綵5?濰鑨�$囶5', 'f010cdccf07caf9d7ea52c80114d3238a9aa63c0', 1),
(28, 'Steve', 'Jbs', 'dragonci0@hotmail.com', '硑C�阔z`�槍-(N', '4af4458b2549953aa68f1c15e8320122fbfe5612', 0),
(29, 'Arturo', 'Del Carpio', 'arturodelcarpio@hotmail.com', '嘩/痪Qm�5qх釁j', 'cfef1f7d46777e2485f97c4998724660ba327cde', 1),
(34, 'Juan', 'Carlos', 'juancarlitos228@gmail.com', '鼤\Z鳿\]M�', '25a1de8b908155d03e94c62c9421421b8aa6562f', 1),
(35, 'Pedro', 'Picapiedra', 'pedropicapiedra@gmail.com', '唘5`�祝盶ZKd韷', '', 0),
(36, 'jose', 'luis', 'joseluis@gmail.com', '鉼滤𚇰忖夏#鱂', '03325bd5231dd047971b354d5ba8fa71a4db423d', 0),
(37, 'Bernab茅', 'Vilcahuaman', 'bernabe@hotmail.com', 'r�&硄蔪T骋镗PE3', '6f9dc4e5edf39f27b7d135c607a5a8deba94dc08', 0),
(38, 'bernabe', 'vilca', 'bernabe.vilcahuaman@outlook.com', '砬Ho#N�''.D甸oc�', '3996150ea0744cc6d39f7eb86019e6f753c25507', 0),
(39, 'Cristhian', 'Toribio Amaro', 'antonio.inkarry@hotmail.com', '�\0[<齇�桧鞈榭焦�?惙郳n闚絜矉L$$芅�渤oM邮^�.埴╘Z�1b錣爪bSr琼蒩Z�餔\0�6o�,厅叒m嫂l钽噚砎妆�:д迋v	Ls|蹞O&籪|�1貟!N埭繺0k \r�4�:篟,韱邉rl� 饳榼)O迖@Y鼿!聁e�&蝐o涟颉V﹑奶I斾據��[痵絻仸殷h��{	醆0蓃山 ', '', 0),
(40, 'Fredy', 'Beteta Acu帽a', 'fredyba_18@hotmail.com', '镁峱�#摕z蔆r卦�<4�d蟘7	嚫崴�鎦L肻\苠g痓DRu�8驶均�W�聎&蒇A鋱栵�''�*�#h�7橚�8冋蠾俓\pJ[�2惴>`鐁嗼鶇�7b这跡羷"�''i駋�;B嘰n/#凲兌POC蕘�慅啊盉�0擅没婃湼f绽zw唂╩�:\r黖駤萼贠4Z�襌i~#�:韝�h^�!瞈ZyWń釐+繹K�+痪', '', 0),
(41, 'miguel', 'ucelli', 'miguelucelli@hotmail.com', '耻-Q�佻5jA�', 'ac0c60032fb44d8c1daa1772f0fc06c4341da28c', 0),
(42, 'Jos茅', 'Luis Tovar', 'talentotutorlider@gmail.com', '砬Ho#N�''.D甸oc�', 'd08472a64fbfaa7ec5d3f7b14416f027476044f3', 1),
(43, 'Jose', 'Tovar', 'j@tutorlider.com', '<�%cF鄁籀d 0�', '3129b52c280d1fbcd5c3797b9369b452a124b9b4', 0),
(44, 'Lizardo', 'Reyes Jara', 'ciberutilidades.universidad@gmail.com', '┧�(甶雫吇彐7bDqm}9r�?@C0<S覝U.Ν`�I举�)n47h消愢�6}.�f>jn鐂鮨膢猑�抲鯊�1FW�.$檏�m唜2F�=钨梆龗!X赁xV�\n款錅黑�,潏g*�u蘵嵪b膋熸槁�$3�-嘰r樵锝v磲楩i�笰~>蟅捛!錙溶c鑬幺q�$吿猩X	欣0粭\Z�s祀J�冽仾Xl,崄', '', 0),
(45, 'Bernab茅', 'Marcelino Medina', 'bernabe@tutorlider.com', '<�%cF鄁籀d 0�', 'a90099cbc7c132026d7991c7530431b213cef919', 0),
(46, 'CArlos', 'Tovar', 'c@tutorlider.com', '<�%cF鄁籀d 0�', '50062f3b50377826577345efe69e343e5b75d46f', 0),
(47, 'Michael', 'Mathews', 'michaelmathewssar@gmail.com', '茩輴	:圚S跻tm仼', 'eccca1550ecaf02ea54369110f0424a69958adff', 1),
(48, 'juan', 'rojas', 'carlos34343434@gmail.com', '暄慍''w睶櫏閇轓�;', '99c6c378f1e47d0666078ba9b08b8948ed9ce012', 0),
(49, 'Jose', 'vilcahuaman', 'jo@tutorlider.com', '鉼滤𚇰忖夏#鱂', 'ef23cd2325d0b5ea509bd79cd512f39b029873cd', 0),
(50, 'juan carlos', 'rojas toralva', 'carlos3434@hotmail.com', '暄慍''w睶櫏閇轓�;', '326b9508092a08a25de321ffe7bd23c80a4ca89a', 1),
(51, 'Carlos', 'Vilca', 'carlos@gmail.com', '撜!�幎F潔栛_眄', '9075dc851eea351aac2970e0cf602bb8cd02a4dd', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE IF NOT EXISTS `clases` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(10) NOT NULL,
  `curso` varchar(200) COLLATE utf8_bin NOT NULL,
  `tema` varchar(200) COLLATE utf8_bin NOT NULL,
  `nivel` varchar(15) COLLATE utf8_bin NOT NULL,
  `archivo` varchar(4) COLLATE utf8_bin NOT NULL,
  `archivo_ruta` varchar(30) COLLATE utf8_bin NOT NULL,
  `tiempo` int(5) NOT NULL,
  `dia` varchar(12) COLLATE utf8_bin NOT NULL,
  `hora` varchar(15) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`id`, `id_alumno`, `curso`, `tema`, `nivel`, `archivo`, `archivo_ruta`, `tiempo`, `dia`, `hora`) VALUES
(1, 6, 'kk', 'jjj', 'universidad', 'si', '', 60, '22-05-2015', '10:30 PM'),
(2, 6, '', '', '', 'no', 'none', 0, '', ''),
(3, 12, 'Matem谩tica 2 - ', 'integrales con variables', 'universidad', 'si', '', 30, '09-06-2015', '10:00 PM'),
(4, 16, 'e', 'e', '', 'si', '', 30, '', ''),
(5, 16, 'matem谩tica', 'ecuaciones', 'colegio', 'si', '', 120, '25-06-2015', '11:30 PM'),
(6, 50, 'test', 'dev', 'universidad', 'si', '', 60, '15-09-2015', '5:00 PM'),
(7, 50, 'test', 'dev', 'universidad', 'si', '', 30, '15-09-2015', '5:30 PM'),
(8, 50, '', '', '', 'no', 'none', 0, '', ''),
(9, 0, '', '', '', 'no', 'none', 0, '', ''),
(10, 50, '', '', '', 'no', 'none', 0, '', ''),
(11, 50, 're', 're', 'universidad', 'si', '', 30, '27-09-2015', '4:00 PM'),
(12, 0, '', '', '', 'no', 'none', 0, '', ''),
(13, 51, 'judsjud', 'qjsujdsu', 'universidad', 'si', '', 30, '30-09-2015', '2:00 AM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_clases`
--

CREATE TABLE IF NOT EXISTS `estado_clases` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_examen` int(10) NOT NULL,
  `id_alumno` int(10) NOT NULL,
  `estado` varchar(20) COLLATE utf8_bin NOT NULL,
  `tutores` varchar(200) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `estado_clases`
--

INSERT INTO `estado_clases` (`id`, `id_examen`, `id_alumno`, `estado`, `tutores`) VALUES
(1, 1, 6, 'por_confirmar', 'Tutores'),
(2, 2, 6, 'por_confirmar', 'Tutores'),
(3, 3, 12, 'por_confirmar', 'Tutores'),
(4, 4, 16, 'por_confirmar', 'Tutores'),
(5, 5, 16, 'por_confirmar', 'Tutores'),
(6, 6, 35, 'por_confirmar', 'Tutores'),
(7, 6, 50, 'por_confirmar', 'Tutores'),
(8, 7, 50, 'por_confirmar', 'Tutores'),
(9, 8, 50, 'por_confirmar', 'Tutores'),
(10, 9, 0, 'por_confirmar', 'Tutores'),
(11, 10, 50, 'por_confirmar', 'Tutores'),
(12, 11, 50, 'por_confirmar', 'Tutores'),
(13, 12, 0, 'por_confirmar', 'Tutores'),
(14, 13, 51, 'por_confirmar', 'Tutores');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
